package com.example.androidappui;

import java.util.ArrayList;
import java.util.List;

import android.R.string;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

@SuppressLint("DrawAllocation")
public class Viewtest2 extends View implements View.OnTouchListener{
    List<Point> points = new ArrayList<Point>();
    Paint paint = new Paint();
    List<Float> floatList  = new ArrayList<Float>();
    boolean isset=false;
    float ther=0;
    boolean islife=true;

    
  //  final ImageButton button01 = (ImageButton) findViewById(R.id.imageButton3);

    
    
    public Viewtest2(Context context) 
    {
        super(context);

        setFocusable(true);
        setFocusableInTouchMode(true);
        this.setOnTouchListener(this);

        
        


    }
    

    @SuppressLint("DrawAllocation")
	@Override
    public void onDraw(Canvas canvas) 
    {
    	// TODO Auto-generated method stub  
        super.onDraw(canvas);  
          

        Paint paint = new Paint();   


 
    	paint.setColor(Color.BLUE);
    	paint.setTextSize(60);
        
        if(isset)
        {

        	canvas.drawText("SET", 250, 250, paint);   
        }
        else
        {

        	canvas.drawText("NOSET", 250, 250, paint);   
        }
                
      
        
        if(!isset)
    	{
        	  paint.setColor(Color.MAGENTA);
              paint.setAntiAlias(true);
        	for(int i = 2; i < floatList.size(); i=i+2)
        	{
        		canvas.drawLine(floatList.get(i-2), floatList.get(i-1), floatList.get(i),  floatList.get(i+1), paint);
        	}
    	}
        else
        {
        	paint.setColor(Color.MAGENTA);
            paint.setAntiAlias(true);
        	//canvas.drawCircle(100, 100,100, paint);
        	if(islife)
        	{
        		canvas.drawCircle(0, getHeight(), ther, paint);
        	}
        	else
        	{
        		canvas.drawCircle(getWidth(),getHeight(), ther, paint);
        	}

        }
        

    }

    public boolean onTouch(View view, MotionEvent event) 
    {
    	if(!isset)
    	{
        Point point = new Point();
        float x,y;
        
         //取得目前觸碰螢幕之x,y值 
        point.x = (int) event.getX();
        point.y = (int) event.getY();
        x =  event.getX();
        floatList .add(x);
        y =  event.getY();
        floatList .add(y);
        

        
        ///* 新增點至Point物件 
        points.add(point);
        
        if(event.getAction()==MotionEvent.ACTION_UP) 
        {
        	
        	isset=true;
        	//球出半徑
        	float getW= getWidth();
        	float getH= getHeight();
        	float fristX= floatList.get(0);
        	float fristY= floatList.get(1);
        	float finalX= floatList.get(floatList.size()-2);
        	float finalY= floatList.get(floatList.size()-1);
        	float m= (finalY-fristY)/(finalX-fristX);
        	float max=0;
        	
        	if(m>=0) //左邊
        	{
        		islife=true;
        		for(int i = 0; i < floatList.size(); i=i+2)
            	{
        			 
        			float s= (floatList.get(i))*(floatList.get(i))+(floatList.get(i+1)-getH)*(floatList.get(i+1)-getH);
        			if(s>max)
        			{
        				max=s;
        			}
            	}
        	}
        	else //右邊
        	{
        		islife=false;
        		for(int i = 0; i < floatList.size(); i=i+2)
            	{
        			float s= (floatList.get(i)-getW)*(floatList.get(i)-getW)+(floatList.get(i+1)-getH)*(floatList.get(i+1)-getH);
        			if(s>max)
        			{
        				max=s;
        			}
            	}
        	}
            ther=(float) Math.sqrt(max);
        }
        
        ///* 用於更新View 
        invalidate();
    	}

        return true;
    }

	

}
