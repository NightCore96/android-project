package com.android.sip.rtlab.teasurebowl;



import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;


import com.android.sip.rtlab.teasurebowl.DragGridView.OnChanageListener;
import com.android.sip.rtlab.teasurebowl.DragGridView.OnMergeListener;
import com.android.sip.rtlab.teasurebowl.DragGridView.OnOutsideListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.SimpleAdapter.ViewBinder;
import android.widget.TextView;
import android.widget.Toast;

//主框架

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint("NewApi")
public class MainActivity extends Activity {

	private int icon_num ;
	private int icon_allnum ;
	//private TableLayout ll_in_sv;
	
	private TbowlDB dbHelper;//通訊錄
	//private CardlistDB  CarddbHelper;//通訊錄 IcondbHelper;//通訊錄
	private SharedPreferences settings;
	
	private int current_type=1;//現在狀態
	
	private TextView hinttext;
	private TextView nothingtext;
	private ProgressBar progressbar;
	private DragGridView maingridView;
	private SimpleAdapter mainadapter;
	
	private ImageView SubRenamebtn;
	private AlertDialog SubAlert;
	private TextView SubName;
	private DragGridView SubdragGridView;
	private SimpleAdapter Subadapter;
	private ArrayList<HashMap<String,Object>> subiconviewlist = new ArrayList<HashMap<String,Object>>();
	
	private ImageView cellphonebtn;
	private ImageView deleteiconbtn;
	private ImageView sharebtn;
	
	private ImageView helpbtn;
	private ImageView newsbtn;
	
	private ArrayList<HashMap<String,Object>> iconviewlist = new ArrayList<HashMap<String,Object>>();
	private HashMap<String,Icondata> iconlist = new HashMap<String,Icondata>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		current_type=CommonUtilities.TYPE_CELLPHONE;
		//通訊錄DB
		dbHelper = new TbowlDB(this);
		//CarddbHelper =new CardlistDB(this);
		//UI 
	//	 ll_in_sv = (TableLayout)findViewById(R.id.ll_in_sv);
	//	 ll_in_sv.removeAllViews();
		 progressbar=  (ProgressBar) findViewById(R.id.progressBar);
		 
		 SubAlert=null;
		
		
		//讀取設定檔部份 
  		settings = getSharedPreferences("shortcut_setting", 0);
		icon_num = settings.getInt("icon_num",-2);//紀錄總有多少ICON
		icon_allnum = settings.getInt("icon_allnum",-2);//紀錄總有多少ICON
		int fristtime = settings.getInt("fristtime",-1);
		if(icon_num==-2)
		{
			icon_num = -1;
			settings.edit()
			.putInt("icon_num", icon_num)
			.commit();
		}
		if(icon_allnum==-2)
		{
			icon_allnum = -1;
			settings.edit()
			.putInt("icon_allnum", icon_allnum)
			.commit();
		}
		if(fristtime==-1)
		{
			fristtime = 0;
			settings.edit()
			.putInt("fristtime", fristtime)
			.commit();
			  AlertDialog d=(AlertDialog)onHelpDialog(true);
		      d.show();
		}
		else
		{
			  AlertDialog d=(AlertDialog)onNewsDialog();
		      d.show();
		}
			
		hinttext= (TextView)findViewById(R.id.hinttext);
		nothingtext= (TextView)findViewById(R.id.nothingtext);
		hinttext.setText("點選即可撥打");
		//	ImageView inco_image= (ImageView)ll.findViewById(R.id.icon);
		sharebtn= (ImageView)findViewById(R.id.sharebtn);
		cellphonebtn= (ImageView)findViewById(R.id.cellphonebtn);
		deleteiconbtn= (ImageView)findViewById(R.id.deleteiconbtn);
		
		
		helpbtn= (ImageView)findViewById(R.id.helpbtn);
		newsbtn= (ImageView)findViewById(R.id.newsbtn);
		
		sharebtn.setOnClickListener( new OnClickListener() {
 		   public void onClick(View v) {
 				current_type=CommonUtilities.TYPE_SHARE;
 				hinttext.setText("點選即可轉化成QRCODE分享");
 				sharebtn.setBackgroundResource(R.drawable.share);
 				cellphonebtn.setBackgroundResource(R.drawable.cellphone_b);
 				deleteiconbtn.setBackgroundResource(R.drawable.deleteicon_b);
 		   }
 		  }
 			);
		
		
		cellphonebtn.setOnClickListener( new OnClickListener() {
	 		   public void onClick(View v) {

	 				current_type=CommonUtilities.TYPE_CELLPHONE;
	 				hinttext.setText("點選即可撥打");
	 				
	 				sharebtn.setBackgroundResource(R.drawable.share_b);
	 				cellphonebtn.setBackgroundResource(R.drawable.cellphone);
	 				deleteiconbtn.setBackgroundResource(R.drawable.deleteicon_b);
	 		   }
	 		  }
	 			);
		
		deleteiconbtn.setOnClickListener( new OnClickListener() {
	 		   public void onClick(View v) {
	 				current_type=CommonUtilities.TYPE_DELETE;
	 				hinttext.setText("點選即可刪除");
	 				sharebtn.setBackgroundResource(R.drawable.share_b);
	 				cellphonebtn.setBackgroundResource(R.drawable.cellphone_b);
	 				deleteiconbtn.setBackgroundResource(R.drawable.deleteicon);
	 		   }
	 		  }
	 			);
		
		
		ImageView moneybtn= (ImageView)findViewById(R.id.moneybtn);
		moneybtn.setOnClickListener( new OnClickListener() {
	 		   public void onClick(View v) {
	 			  AlertDialog d=(AlertDialog)onMoneylistDialog();
			      d.show();
	 		   }
	 		  }
	 			);
		
		helpbtn.setOnClickListener( new OnClickListener() {
	 		   public void onClick(View v) {
		 			  AlertDialog d=(AlertDialog)onHelpDialog(false);
				      d.show();
	 		   }
	 		  }
	 			);
		newsbtn.setOnClickListener( new OnClickListener() {
	 		   public void onClick(View v) {
		 			  AlertDialog d=(AlertDialog)onNewsDialog();
				      d.show();
	 		   }
	 		  }
	 			);
		
		loadiconlist();
		//showiconlist();

	}
	
	
	//讀取資料庫裡面的ICON資料並放到UI上面
	@SuppressLint("NewApi")
	private void loadiconlist() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() 
		{

			@Override
			protected Void doInBackground(Void... params) {
				ProgressBar progressbar=  (ProgressBar) findViewById(R.id.progressBar);
				progressbar.setVisibility(View.VISIBLE);
				// TODO Auto-generated method stub
				Cursor cursor = dbHelper.getAllIcon();	//取得SQLite類別的回傳值:Cursor物件
				int rows_num = cursor.getCount();	//取得資料表列數
				if(rows_num != 0) 
				{
					cursor.moveToFirst();			//將指標移至第一筆資料
					for(int i=0; i<rows_num; i++) 
					{
						int id = cursor.getInt(1);	//取得第0欄的資料，根據欄位type使用適當語法
						String url = cursor.getString(2);
						String card = cursor.getString(3);
						String to = cursor.getString(4);
						String from = cursor.getString(5);
						String name = cursor.getString(6);
						String icon = cursor.getString(7);
						int position = cursor.getInt(8);
						int sort =cursor.getInt(9);
						Icondata t =new Icondata(id,url,card,to,from,name,icon,position,sort);
						//LOG
						Log.i("id", String.valueOf(t.id));
						Log.i("url", t.url);
						Log.i("card", t.card);
						Log.i("to", t.to);
						Log.i("from", t.from);
						Log.i("name", t.name);
						Log.i("icon", t.icon);
						Log.i("position", String.valueOf(t.position));
						Log.i("sort", String.valueOf(t.sort));
						Log.i("============", "=============");
						cursor.moveToNext();
					}
				}
				cursor.close();
				if(rows_num != 0) 
				{
					//cursor.moveToFirst();			//將指標移至第一筆資料
					nothingtext.setVisibility(View.INVISIBLE);
					hinttext.setVisibility(View.VISIBLE);
					for(int i=0; i<icon_num+1; i++) 
					{
						Cursor  c=dbHelper.getIcon( -1,i);//取得SQLite類別的回傳值:Cursor物件
				    	int r = c.getCount();	
				    	Log.i("dbHelper", String.valueOf(r));

				    	
				    //	c.moveToFirst();	

//				    
						int id = c.getInt(1);	//取得第0欄的資料，根據欄位type使用適當語法
						String url = c.getString(2);
						String card = c.getString(3);
						String to = c.getString(4);
						String from = c.getString(5);
						String name = c.getString(6);
						String icon = c.getString(7);
						int position = c.getInt(8);	
						int sort =c.getInt(9);
						Icondata t =new Icondata(id,url,card,to,from,name,icon,position,sort);
						iconlist.put(String.valueOf(t.id),t); //放入資料庫
						
				    	c=dbHelper.getIconbypostion(id);	//抓取該項目在主業面的顯示
				    	int s = c.getCount();	
				    	Log.i("dbHelper+", String.valueOf(s));
				    	Log.i("dbHelper+id", String.valueOf(id));

				    	c.close();
				    	if( s==0)
				    	{
							Bitmap iconbitmap =CommonUtilities.getBitmapFromURL("http://"+t.icon,MainActivity.this);
							CardIfonData cardIfon= WebService.getCardcurrentPoint(t.card);
							HashMap<String,Object> item = new HashMap<String,Object>();
							item.put("icon",iconbitmap);
							if(cardIfon.currentPoint>-1)
								item.put("cardvalue",String.valueOf(cardIfon.currentPoint));
							else
								item.put("cardvalue","");
							item.put("name",t.name);
							iconviewlist.add(item);
				    	}
				    	else if(s>0)
				    	{
							Bitmap iconbitmap =BitmapFactory.decodeResource(getResources(), R.drawable.subdocm);
							if(s>=4)
							{
								Cursor c0=dbHelper.getIcon( id,0);
								Bitmap iconbitmap0 =CommonUtilities.getBitmapFromURL("http://"+c0.getString(7),MainActivity.this);
								c0.close();
								Cursor c1=dbHelper.getIcon( id,1);
								Bitmap iconbitmap1 =CommonUtilities.getBitmapFromURL("http://"+c1.getString(7),MainActivity.this);
								c1.close();
								Cursor c2=dbHelper.getIcon( id,2);
								Bitmap iconbitmap2 =CommonUtilities.getBitmapFromURL("http://"+c2.getString(7),MainActivity.this);
								c2.close();
								Cursor c3=dbHelper.getIcon( id,3);
								Bitmap iconbitmap3 =CommonUtilities.getBitmapFromURL("http://"+c3.getString(7),MainActivity.this);
								c3.close();
								iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
								iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.05);
								iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap2,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.55);
								iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap3,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.55);
								
							}
							else if (s==3)
							{
								Cursor c0=dbHelper.getIcon( id,0);
								Bitmap iconbitmap0 =CommonUtilities.getBitmapFromURL("http://"+c0.getString(7),MainActivity.this);
								c0.close();
								Cursor c1=dbHelper.getIcon( id,1);
								Bitmap iconbitmap1 =CommonUtilities.getBitmapFromURL("http://"+c1.getString(7),MainActivity.this);
								c1.close();
								Cursor c2=dbHelper.getIcon( id,2);
								Bitmap iconbitmap2 =CommonUtilities.getBitmapFromURL("http://"+c2.getString(7),MainActivity.this);
								c2.close();
								iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
								iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.05);
								iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap2,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.55);
							}
							else if (s==2)
							{
								Cursor c0=dbHelper.getIcon( id,0);
								Bitmap iconbitmap0 =CommonUtilities.getBitmapFromURL("http://"+c0.getString(7),MainActivity.this);
								c0.close();
								Cursor c1=dbHelper.getIcon( id,1);
								Bitmap iconbitmap1 =CommonUtilities.getBitmapFromURL("http://"+c1.getString(7),MainActivity.this);
								c1.close();
								iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
								iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.05);
								
							}
							else if (s==1)
							{
								Cursor c0=dbHelper.getIcon( id,0);
								Bitmap iconbitmap0 =CommonUtilities.getBitmapFromURL("http://"+c0.getString(7),MainActivity.this);
								c0.close();
								iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
							}
							HashMap<String,Object> item = new HashMap<String,Object>();
							item.put("icon",iconbitmap);
							item.put("cardvalue","");
							item.put("name",t.name);
							iconviewlist.add(item);
				    	}
				    	else
				    	{
				    		 Toast.makeText(MainActivity.this, "資料庫出現錯誤", Toast.LENGTH_LONG).show();
				    	}
				
					}
				}
				return null;
			}
			protected void onPostExecute(Void result)
			{
				mainadapter= new SimpleAdapter(MainActivity.this, 
						iconviewlist, R.layout.icon_layout, new String[]{"icon", "cardvalue","name"},
					    new int[]{R.id.icon, R.id.cardvalue,R.id.name});
				mainadapter.setViewBinder(new ViewBinder() {

			        @Override
			        public boolean setViewValue(View view, Object data,
			            String textRepresentation) {
			          if (view instanceof ImageView && data instanceof Bitmap) {
			            ImageView iv = (ImageView) view;
			        	Bitmap scaledBitmap= Bitmap.createScaledBitmap((Bitmap) data, 192, 192, true);
			            iv.setImageBitmap(scaledBitmap);
			            return true;
			          }
			          if (view instanceof TextView && data instanceof String) {
			        	  TextView tv = (TextView) view;
			        	  if(String.valueOf(data)=="")
			        	  {
			        		  tv.setVisibility(View.INVISIBLE);
			        	  }
			        	  else
			        	  {
			        		  tv.setVisibility(View.VISIBLE);
			        		  tv.setText((CharSequence) data);
			        	  }
				            return true;
				          }
			          return false;
			        }
			      });
				maingridView = (DragGridView)findViewById(R.id.main_page_gridview);
				maingridView.setAdapter(mainadapter);
				maingridView.setOnItemClickListener(new OnItemClickListener(){
				    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
				    {
				    	Cursor c = dbHelper.getIcon(-1,position);	//取得SQLite類別的回傳值:Cursor物件
				    	
				    	int cid = c.getInt(1);
				    	Log.i("cid", String.valueOf(cid));
				     	Cursor cursor = dbHelper.getIconbypostion(cid);	//取得SQLite類別的回傳值:Cursor物件
				    	int rows_num = cursor.getCount();
				     	if(rows_num == 0)
		 			  	{
							int cardid = c.getInt(1);	//取得第0欄的資料，根據欄位type使用適當語法
							String url = c.getString(2);
							String card = c.getString(3);
							String to = c.getString(4);
							String from = c.getString(5);
							String name = c.getString(6);
							String icon = c.getString(7);
							int iconposition = c.getInt(8);	
							int iconsort = c.getInt(9);	
							Bitmap iconbitmap =(Bitmap) iconviewlist.get(position).get("icon");
							 showiconmeun(cid,cardid,url,name,icon,card,to,from,iconposition,iconsort,iconbitmap);

						
		 			  	}
				     	else if(rows_num>=1) //資料夾
				     	{
				     		SubAlert=(AlertDialog) onSubDocmentDialog(cid);
				
				     		SubAlert.show();
				     		//Toast.makeText(MainActivity.this, "你按下第"+position+"個", Toast.LENGTH_LONG).show();
				     	}
				     	else
				     	{
				     		 Toast.makeText(MainActivity.this, "資料庫出現錯誤", Toast.LENGTH_LONG).show();
				     	}
				     	cursor.close();
				     	c.close();
				    }
				            
				});
				maingridView.setOnChangeListener(new OnChanageListener()
				{
					 public void onChange(int from, int to) {  
			                HashMap<String, Object> temp = iconviewlist.get(from);  

			                  
			                if(from < to){  
			                    for(int i=from; i<to; i++){  
			                        Collections.swap(iconviewlist, i, i+1);  
			                        dbHelper.renewIconPostion(i,-2); 
			                        dbHelper.renewIconPostion(i+1, i); 
			                        dbHelper.renewIconPostion(-2, i+1);
			                    }  
			                }else if(from > to){  
			                    for(int i=from; i>to; i--){  
			                        Collections.swap(iconviewlist, i, i-1);  
			                        dbHelper.renewIconPostion(i,-2); //a=t
			                        dbHelper.renewIconPostion(i-1, i); 
			                        dbHelper.renewIconPostion(-2, i-1);
			                    }  
			                }  
			                  
			                iconviewlist.set(to, temp);  
			                  
			                mainadapter.notifyDataSetChanged();  
			                  
			                  
			            }  
				});
				maingridView.setOnMergeListener(new OnMergeListener()
				{

					@Override
					public void OnMerge(int form, int to) 
					{
						// TODO Auto-generated method stub
						Cursor cursor = dbHelper.getIcon(-1,form);	//取得SQLite類別的回傳值:Cursor物件
						int cpostion =cursor.getInt(1);
						cursor.close();
						Cursor cp = dbHelper.getIconbypostion(cpostion);	//取得SQLite類別的回傳值:Cursor物件
						int rows_num =cp.getCount();
						cp. close();

						if(rows_num==0) //被拖曳的是ICON才可以合併阿
						{
				//			 Toast.makeText(MainActivity.this, "setOnMergeListener", Toast.LENGTH_LONG).show();
							
						     
						     //將要被合併項目改成子資料夾裡的項目
								Cursor c= dbHelper.getIcon(-1,to);	//取得SQLite類別的回傳值:Cursor物件
						    	int r = c.getCount();
						    	int cid =c.getInt(1);
						    	
						    	c = dbHelper.getIconbypostion(cid);	//取得SQLite類別的回傳值:Cursor物件
						    	r = c.getCount();
						    	c.close();
						    	if(r==0)//如果是ICON才要合併 並新增項目
						    	{
						    		
						    						 			  		icon_allnum++;
				 			  		settings.edit()
				 					.putInt("icon_allnum", icon_allnum)
				 					.commit();
						    		dbHelper.renewIconPostion(-1, to, icon_allnum, 0);
						    		dbHelper.renewIconPostion(-1, form, icon_allnum , 1);
						    		
						    		Log.i("icon_allnum", String.valueOf(icon_allnum));
						    		dbHelper.createIcon(icon_allnum, "", "", "", "", "電話群組", "", -1,to);
								     HashMap<String, Object> temp = new HashMap<String,Object>();  
								     Bitmap iconbitmap =BitmapFactory.decodeResource(getResources(), R.drawable.subdocm);
								    
									Bitmap iconbitmap0 =(Bitmap) iconviewlist.get(to).get("icon");
									Bitmap iconbitmap1 =(Bitmap) iconviewlist.get(form).get("icon");

									iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
									iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.05);
									
								     temp.put("icon",iconbitmap);
								     temp.put("cardvalue","");
								     temp.put("name","電話群組");
						    		 iconviewlist.set(to, temp);  
						    	}
						    	else if(r>0)
						    	{
						    		dbHelper.renewIconPostion(-1, form, cid, r);
						    		Log.i("dbHelper.renewIconPostion(-1, form, cid, r);", "dbHelper.renewIconPostion(-1,"+ String.valueOf(form)+","+ String.valueOf(cid)+", "+ String.valueOf(r)+");");
						    		if(r==1) //
						    		{
						    		   HashMap<String, Object> temp = new HashMap<String,Object>();  
									    Bitmap iconbitmap =(Bitmap) iconviewlist.get(to).get("icon");
										Bitmap iconbitmap1 =(Bitmap) iconviewlist.get(form).get("icon");

										
										iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1, CommonUtilities.ICON_SCALE,CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.05);
										
									     temp.put("icon",iconbitmap);
									     temp.put("cardvalue","");
									     temp.put("name",iconviewlist.get(to).get("name"));
							    		 iconviewlist.set(to, temp);  
						    		}
						    		if(r==2)
						    		{
						    		   HashMap<String, Object> temp = new HashMap<String,Object>();  
									    Bitmap iconbitmap =(Bitmap) iconviewlist.get(to).get("icon");
										Bitmap iconbitmap1 =(Bitmap) iconviewlist.get(form).get("icon");

										
										iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1,CommonUtilities.ICON_SCALE,CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.55);
										
									     temp.put("icon",iconbitmap);
									     temp.put("cardvalue","");
									     temp.put("name",iconviewlist.get(to).get("name"));
							    		 iconviewlist.set(to, temp);  
						    		}
						    		if(r==3)
						    		{
						    		   HashMap<String, Object> temp = new HashMap<String,Object>();  
									    Bitmap iconbitmap =(Bitmap) iconviewlist.get(to).get("icon");
										Bitmap iconbitmap1 =(Bitmap) iconviewlist.get(form).get("icon");

										
										iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1, CommonUtilities.ICON_SCALE,CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.55);
										
									     temp.put("icon",iconbitmap);
									     temp.put("cardvalue","");
									     temp.put("name",iconviewlist.get(to).get("name"));
							    		 iconviewlist.set(to, temp);  
						    		}
						    		
						    	}
						     
						     
						     
						     
						   
					     
						     //移除被合併的ICON
							for(int i =form;i<icon_num;i++)
							{
								dbHelper.renewIconPostion(-1,i+1,-1, i);
								Log.i("dbHelper.renewIconPostion(-1,i+1,-1, i);", "dbHelper.renewIconPostion(-1,"+String.valueOf(i+1) +","+ "-1"+", "+String.valueOf(i)+");");
							}
							icon_num--;
					  		settings.edit()
							.putInt("icon_num", icon_num)
							.commit();
						     iconviewlist.remove(form);
						     
						     
						     
						     mainadapter.notifyDataSetChanged();  
						}
					  
						 
					}
					
				});
				
				maingridView.setMergeBG(BitmapFactory.decodeResource(getResources(), R.drawable.mergebg));
				

				progressbar.setVisibility(View.GONE);
				  super.onPostExecute(result);
			}
			
		}.execute();
	
		
	}



	protected void showiconmeun(int cid,int id, String url, String name, String icon,String card, String to, String from,int position,int sort,Bitmap iconimage) {

			switch(current_type)
			{
			case CommonUtilities.TYPE_CELLPHONE:
				if(to.isEmpty()||from.isEmpty())
				{
				  if(to.isEmpty()&&from.isEmpty())
				  {
					  AlertDialog d=(AlertDialog) ondoubleinputDialog(cid );
					   d.show();
				  }
				   else if(to.isEmpty())
				   {
					   
					   AlertDialog d=(AlertDialog)oninputDialog("尚未綁定電話","請輸入你的電話" ,1,cid);
					   d.show();
					  }
					else if(from.isEmpty())
					{
						   AlertDialog d=(AlertDialog)oninputDialog("尚未綁定電話","請輸入對方電話" ,2,cid);
						   d.show();
					}
				}
				else
				{
					cellphone("http://"+url+"/call.php?ac="+card+"&from="+ from+"&to="+to,name);
				}
				break;
			case CommonUtilities.TYPE_DELETE:
				 AlertDialog d=(AlertDialog)onDeleteDialog(id,name,position,sort);
				 d.show();
				break;
			case CommonUtilities.TYPE_SHARE:
				 AlertDialog c=(AlertDialog)encodetoHGqr(id,url,name,icon,card,to,from);
				 c.show();
				//encodetoqr(id,url,name,icon,card,to,from,iconimage,1);
				break;
			}
//			current_type=CommonUtilities.TYPE_CELLPHONE;
//			hinttext.setText("點選即可撥打");
		// TODO Auto-generated method stub
		
	}
		//qr code form HG qr code 
		public Dialog  encodetoHGqr(final int id,final String url,final String name, final String icon,final String card,final String to,final String from) 
		{
		    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		    // Get the layout inflater
		    LayoutInflater inflater = MainActivity.this.getLayoutInflater();
		    final View dialoglayout = inflater.inflate(R.layout.qrcodeshow_layout, null);
		    final ImageView  QRimage = (ImageView )(dialoglayout.findViewById(R.id.QRcodeview));
			final ProgressBar webloadprogressBar= (ProgressBar)dialoglayout. findViewById(R.id.webloadprogressBar);
			final String path = Environment.getExternalStorageDirectory().toString()+"/teasurebowl";
			final String	 filename="QRtamp"+CommonUtilities.getTimeName()+".jpg" ;
			new AsyncTask<Void, Void, Bitmap>() 
			{

				@Override
				protected Bitmap doInBackground(Void... params) {
					
					QRimage.setVisibility(View.INVISIBLE);
					webloadprogressBar.setVisibility(View.VISIBLE);
					String iconlink=icon;
					//Log.i(" icon.substring(icon.length() - 1)", icon.substring(icon.length() - 1));
					if( icon.substring(icon.length() - 1).equals("/"))
					{
						iconlink=icon.substring(0, icon.length()-1);
						//Log.i(" icon.substring(icon.length() - 1)", icon.substring(icon.length() - 1));
					}
					String[] AfterSplit = iconlink.split("/");
					Log.i("0", AfterSplit[0]);
					Log.i("0", AfterSplit[1]);
					Log.i("0", AfterSplit[2]);
					Log.i("0", AfterSplit[3]);
					Log.i("QRcodeURL","http://140.114.79.118:8000/qrcodegen/?card="+card+"&from="+from+"&to="+to+"&NAME="+name+"&ICON="+iconlink);
					//Bitmap QRcode = CommonUtilities.getBitmapFromURL("http://140.114.79.118:8000/qrcodegen/?card="+card+"&from="+from+"&to="+to+"&NAME="+name+"&ICON="+iconlink, MainActivity.this);
					Bitmap QRcode =CommonUtilities.getBitmapFromURL("http://"+CommonUtilities.IMAGEWEB_URL+"qrcode_"+AfterSplit[3],MainActivity.this);
					// TODO Auto-generated method stub
					return QRcode;
				}
				protected void onPostExecute(Bitmap result)
				{
					
					QRimage.setImageBitmap(result);

	            	if(CommonUtilities.checkSDCard())
	   				{
	   				    if(!CommonUtilities.checkSDCardisFull(CommonUtilities.Bitmap2Bytes(result)))
	   				    {
	   						 try {
	   							CommonUtilities.saveBitmap(path, result,filename);
	   						} catch (IOException e) {
	   							// TODO Auto-generated catch block
	   							Toast.makeText(MainActivity.this, 	e.toString(), Toast.LENGTH_LONG).show();
	   							e.printStackTrace();
	   						}
	   					//	 Toast.makeText(MainActivity.this, "已轉換成QRCode存至"+path+card+"_"+name+".png", Toast.LENGTH_LONG).show();
	   				    }
	   					else
	   					{
	   						Toast.makeText(MainActivity.this, "ERROR : SD卡大小不夠", Toast.LENGTH_LONG).show();
	   					}
	   				}
	   				else
	   				{
	   					Toast.makeText(MainActivity.this, "ERROR : 找不到SD卡", Toast.LENGTH_LONG).show();
	   				}
					
					QRimage.setVisibility(View.VISIBLE);
					webloadprogressBar.setVisibility(View.INVISIBLE);
					
				}
			}.execute();
			
		    builder.setTitle("分享QRCode");
		    builder.setView(dialoglayout)
		    // Add action buttons
		           .setPositiveButton("使用其他應用程式傳送", new DialogInterface.OnClickListener() {
		               @Override
		               public void onClick(DialogInterface dialog, int id) {	
		            	  ShareQRCodetoFriend(path+File.separator+filename,name);
		            	  // EditText  email = (EditText )(dialoglayout.findViewById(R.id.email));
		               }
		           })
		           .setNegativeButton("取消", new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		                   //LoginDialogFragment.this.getDialog().cancel();
		               }
		           });      
		    return builder.create();
			
		}
		
		//qr code form ZXing lib
		private void encodetoqr(int id, String url, String name, String icon,String card, String to, String from,Bitmap iconimage,int type) {
			// TODO Auto-generated method stub
			try {
				Bitmap qrcode=null;
				if(type==1)
				{
					//
					qrcode=Zxingcoder.encodeAsBitmapWithImage("http://rtlabdemotest?URL="+url+"&card="+card+"&from="+ from+"&to="+to+"&NAME="+name+"&ICON="+icon, BarcodeFormat.QR_CODE, 750, 750,iconimage);
					//qrcode=Zxingcoder.encodeAsBitmap("URL="+url+"&card="+card+"&from="+ from+"&to="+to+"&NAME="+name+"&ICON="+icon, BarcodeFormat.QR_CODE, 750, 750);
				}
				
				else if(type==2)
				{
					 qrcode=Zxingcoder.encodeAsBitmapWithImage("http://rtlabdemotest?giftcard="+card, BarcodeFormat.QR_CODE, 750, 750,iconimage);
				}
				String path;
				String filename;
				//Bitmap back=BitmapFactory.decodeResource(getResources(), R.drawable.qrcode_backend);
				//Bitmap save=CommonUtilities.combineBitmap(back, qrcode);
				if(CommonUtilities.checkSDCard())
				{
				    if(!CommonUtilities.checkSDCardisFull(CommonUtilities.Bitmap2Bytes(qrcode)))
				    {
						 path = Environment.getExternalStorageDirectory().toString()+"/teasurebowl";
						 filename="QRtamp"+CommonUtilities.getTimeName()+".jpg" ;
						 try {
							CommonUtilities.saveBitmap(path, qrcode,filename);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							Toast.makeText(MainActivity.this, 	e.toString(), Toast.LENGTH_LONG).show();
							e.printStackTrace();
						}
					//	 Toast.makeText(MainActivity.this, "已轉換成QRCode存至"+path+card+"_"+name+".png", Toast.LENGTH_LONG).show();
						 AlertDialog d=(AlertDialog)onCreateQRDialog(path+File.separator+filename,qrcode,name);
						 d.show();
				    }
					else
					{
						Toast.makeText(MainActivity.this, "ERROR : SD卡大小不夠", Toast.LENGTH_LONG).show();
					}
				}
				else
				{
					Toast.makeText(MainActivity.this, "ERROR : 找不到SD卡", Toast.LENGTH_LONG).show();
				}
			} catch (WriterException e) {
				// TODO Auto-generated catch block
				Toast.makeText(MainActivity.this,e.toString(), Toast.LENGTH_LONG).show();
				e.printStackTrace();
			} 
		}

      //分享QR CODE
		private void ShareQRCodetoFriend(String string, String name) {
			// TODO Auto-generated method stub
		     Uri imageUri = Uri.parse("file:///"+string);
		      Intent intent = new Intent(Intent.ACTION_SEND);
		      intent.setType("image/jpeg");
     	      intent.putExtra(Intent.EXTRA_STREAM, imageUri);
     	//     intent.setType("text/*");
	          intent.putExtra(Intent.EXTRA_TEXT, "聚寶盆ICON "+name);
		      startActivity(Intent.createChooser(intent , "分享這個QRCode出去吧"));
		}

		//刪除ICON
		protected void deleteICON(int id ,int position,int sort) {
		// TODO Auto-generated method stub
			if(position==-1) //主畫面
			{
				if( dbHelper.deleteIcon(id))
				{
						for(int i =sort;i<icon_num;i++)
						{
							dbHelper.renewIconPostion(position,i+1,position, i);
						}
						icon_num--;
				  		settings.edit()
						.putInt("icon_num", icon_num)
						.commit();
				  		iconviewlist.remove(sort);
				  		mainadapter.notifyDataSetChanged();
				  		if(iconviewlist.size()<=0)
				  		{
				  			nothingtext.setVisibility(View.VISIBLE);
				  			hinttext.setVisibility(View.INVISIBLE);
				  		}
				  		
				  		
				}
			}
			else//子畫面
			{
				if(dbHelper.deleteIcon(id))
				{
					int csort=dbHelper.getIcon(position).getInt(9);
					subiconviewlist.remove(sort);
					if(subiconviewlist.size()==4)
					{
 			  			HashMap<String, Object> maintemp = iconviewlist.get(csort);//主頁面資料夾要重作
	 			  		Bitmap iconbitmap =BitmapFactory.decodeResource(getResources(), R.drawable.subdocm);
	 			  		maintemp.put("icon", iconbitmap);
	 			  		
						Bitmap iconbitmap0 =(Bitmap) subiconviewlist.get(0).get("icon");
						Bitmap iconbitmap1 =(Bitmap) subiconviewlist.get(1).get("icon");
						Bitmap iconbitmap2 =(Bitmap) subiconviewlist.get(2).get("icon");
						Bitmap iconbitmap3 =(Bitmap) subiconviewlist.get(3).get("icon");
						
						iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
						iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.05);
						iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap2,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.55);
						iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap3,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.55);
						maintemp.put("icon", iconbitmap);
						iconviewlist.set(csort, maintemp);
					}
					else if (subiconviewlist.size()==3)
					{
						HashMap<String, Object> maintemp = iconviewlist.get(csort);//主頁面資料夾要重作
	 			  		Bitmap iconbitmap =BitmapFactory.decodeResource(getResources(), R.drawable.subdocm);
	 			  		maintemp.put("icon", iconbitmap);
	 			  		
						Bitmap iconbitmap0 =(Bitmap) subiconviewlist.get(0).get("icon");
						Bitmap iconbitmap1 =(Bitmap) subiconviewlist.get(1).get("icon");
						Bitmap iconbitmap2 =(Bitmap) subiconviewlist.get(2).get("icon");
						
						iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
						iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.05);
						iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap2,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.55);
					
						maintemp.put("icon", iconbitmap);
						iconviewlist.set(csort, maintemp);
					}
					else if (subiconviewlist.size()==2)
					{
						HashMap<String, Object> maintemp = iconviewlist.get(csort);//主頁面資料夾要重作
	 			  		Bitmap iconbitmap =BitmapFactory.decodeResource(getResources(), R.drawable.subdocm);
	 			  		maintemp.put("icon", iconbitmap);
	 			  		
						Bitmap iconbitmap0 =(Bitmap) subiconviewlist.get(0).get("icon");
						Bitmap iconbitmap1 =(Bitmap) subiconviewlist.get(1).get("icon");
						
						iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
						iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.05);
						maintemp.put("icon", iconbitmap);
						iconviewlist.set(csort, maintemp);
						
					}
					else if (subiconviewlist.size()==1)
					{
						HashMap<String, Object> maintemp = iconviewlist.get(csort);//主頁面資料夾要重作
	 			  		Bitmap iconbitmap =BitmapFactory.decodeResource(getResources(), R.drawable.subdocm);
	 			  		maintemp.put("icon", iconbitmap);
	 			  		
						Bitmap iconbitmap0 =(Bitmap) subiconviewlist.get(0).get("icon");
						
						iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
						maintemp.put("icon", iconbitmap);
						iconviewlist.set(csort, maintemp);
					}
					mainadapter.notifyDataSetChanged();
					Subadapter.notifyDataSetChanged();  
					int r=dbHelper.getIconbypostion(position).getCount();
					if(r==0)//當最後只剩下資料夾本身
					{
						deleteICON(position,-1,csort);
						if(SubAlert!=null)
						{
							if(this.SubAlert.isShowing())	
								this.SubAlert.dismiss();
							SubAlert=null;
						}
					}
					for(int i =sort;i<r+1;i++)
					{
						dbHelper.renewIconPostion(position,i+1,position, i);
					}
				}
				
				

				
			}
		}

       //撥打電話
		protected void cellphone(String url,String name) {
			// TODO Auto-generated method stub
			Intent intent = new Intent();
			intent.setClass(MainActivity.this, WebActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("URL", url);
			bundle.putString("NAME", name);
			intent.putExtras(bundle);
			startActivity(intent); 

			MainActivity.this.finish(); 
		}
		
		//創造QR CODE 彈跳選單
		@SuppressLint("InflateParams")
		public Dialog onCreateQRDialog(final String link,Bitmap qr,final String name) 
		{
		    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		    // Get the layout inflater
		    LayoutInflater inflater = MainActivity.this.getLayoutInflater();
		    final View dialoglayout = inflater.inflate(R.layout.qrcodeshow_layout, null);
		    ImageView  QRimage = (ImageView )(dialoglayout.findViewById(R.id.QRcodeview));
		    QRimage.setImageBitmap(qr);
		    // Inflate and set the layout for the dialog
		    // Pass null as the parent view because its going in the dialog layout
		    builder.setTitle("QRCode產生完成");
		    builder.setView(dialoglayout)
		    // Add action buttons
		           .setPositiveButton("使用其他應用程式傳送", new DialogInterface.OnClickListener() {
		               @Override
		               public void onClick(DialogInterface dialog, int id) {	
		            	   ShareQRCodetoFriend(link,name);
		            	  // EditText  email = (EditText )(dialoglayout.findViewById(R.id.email));
		               }
		           })
		           .setNegativeButton("取消", new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		                   //LoginDialogFragment.this.getDialog().cancel();
		               }
		           });      
		    return builder.create();
		}

		//是否刪除 彈跳選單
		public Dialog onDeleteDialog(final int iconid,String name,final int position,final int sort) 
		{
		    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

		    
		    // Inflate and set the layout for the dialog
		    // Pass null as the parent view because its going in the dialog layout
		    builder.setTitle("是否要刪除 "+name+" 這個Icon");
		    
		  
		    // Add action buttons
		    builder.setPositiveButton("刪除", new DialogInterface.OnClickListener() {
		               @Override
		               public void onClick(DialogInterface dialog, int id) {	
		            	   
		            	   Log.i("iconid ", String.valueOf(iconid));
		            	   Log.i("position ", String.valueOf(position));
		            	   Log.i("position ", String.valueOf(sort));
		            	   deleteICON(iconid,position,sort);
		            	  // EditText  email = (EditText )(dialoglayout.findViewById(R.id.email));
		               }
		           })
		           .setNegativeButton("取消", new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		                   //LoginDialogFragment.this.getDialog().cancel();
		               }
		           });      
		    return builder.create();
		}
		
		//說明的 彈跳選單
		@SuppressLint({ "InflateParams", "SetJavaScriptEnabled" })
		public Dialog onHelpDialog(boolean first) 
		{
			LayoutInflater inflater = MainActivity.this.getLayoutInflater();
			final View dialoglayout = inflater.inflate(R.layout.help_layout, null);
		    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		    WebView helpweb=(WebView)dialoglayout.findViewById(R.id.helpweb);  
		    WebSettings websettings = helpweb.getSettings();  
	        websettings.setJavaScriptEnabled(true); 
	        websettings.setUseWideViewPort(true); 
	        websettings.setLoadWithOverviewMode(true); 
	        helpweb.loadUrl(CommonUtilities.HTTP+CommonUtilities.HELP_URL);  
	        builder.setView(dialoglayout);
	        if(!first)
	        {
	        builder.setNegativeButton("關閉", new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	                  
	               }
	           });      
	        }
	        else
	        {
		        builder.setNegativeButton("關閉", new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
				 			  AlertDialog d=(AlertDialog)onNewsDialog();
						      d.show();
		               }
		           });      
	        }
		    return builder.create();
		}
		
		//最新消息 彈跳選單
		@SuppressLint({ "InflateParams", "SetJavaScriptEnabled" })
		public Dialog onNewsDialog() 
		{
			LayoutInflater inflater = MainActivity.this.getLayoutInflater();
			final View dialoglayout = inflater.inflate(R.layout.help_layout, null);
		    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		    WebView helpweb=(WebView)dialoglayout.findViewById(R.id.helpweb);  
		    WebSettings websettings = helpweb.getSettings();  
	        websettings.setJavaScriptEnabled(true); 
	        websettings.setUseWideViewPort(true); 
	        websettings.setLoadWithOverviewMode(true); 
	        helpweb.loadUrl(CommonUtilities.HTTP+CommonUtilities.NEWS_URL);  
	        builder.setView(dialoglayout);
	        builder.setNegativeButton("關閉", new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	                   //LoginDialogFragment.this.getDialog().cancel();
	               }
	           });      
		    return builder.create();
		}
		
		//庫存金的彈跳選單
		@SuppressLint("InflateParams")
		public Dialog onMoneylistDialog() 
		{
			LayoutInflater inflater = MainActivity.this.getLayoutInflater();
			final View dialoglayout = inflater.inflate(R.layout.moneylist_layout, null);
		    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		    final LinearLayout ll_in_sv_cardlist;
		    ll_in_sv_cardlist =(LinearLayout) dialoglayout.findViewById(R.id.ll_in_sv_cardlist);
		    ll_in_sv_cardlist.removeAllViews();
		    
		    Cursor cursor = dbHelper.getAllCard();	//取得SQLite類別的回傳值:Cursor物件
			int rows_num = cursor.getCount();	//取得資料表列數
			if(rows_num != 0) 
			{
				cursor.moveToFirst();			//將指標移至第一筆資料
				for(int i=0; i<rows_num; i++) 
				{
					String cardID = cursor.getString(1);
				    new AsyncTask<String, Void, View>() 
			        {
			          @Override
			          protected View doInBackground(String... params) 
			          {
			        	  try{
			        		  return addCardlist(params[0],false);
			        	  }catch(Exception e)
			        	  {
			        		  Log.e("ERROR", e.getMessage());
			        		  dbHelper.deleteCard(params[0]);
			        		  return null;
			        	  }
			          }
			          protected void onPostExecute(View result) 
			          {
			        	  if(result ==null)
			        	  {
			        		  //Toast.makeText(MainActivity.this, "資料庫出現錯誤", Toast.LENGTH_LONG).show();
			        	  }	 
			        	  else
			        	  {
			        		  ll_in_sv_cardlist.addView(result);
			        	  }
			        
			        	  super.onPostExecute(result);
			          }       
			        }.execute(cardID);
			        cursor.moveToNext();
				}
			}
		   

	
		    // Inflate and set the layout for the dialog
		    // Pass null as the parent view because its going in the dialog layout
		    builder.setTitle("你的庫存金");
		    
		    builder.setView(dialoglayout);
		    // Add action buttons
		    builder.setPositiveButton("存入或分享庫存金", new DialogInterface.OnClickListener() {
		               @Override
		               public void onClick(DialogInterface dialog, int id) {	
		            	   AlertDialog d =(AlertDialog)onManualnputDialog();
			            	d.show();
		               }
		           })
		           .setNegativeButton("取消", new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		               }
		           });      
		    return builder.create();
		}
		
		//讀出庫存金
		@SuppressLint("InflateParams")
		protected View addCardlist(String cardID ,boolean click) {
			// TODO Auto-generated method stub
     		View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.card_layout, null); //物件
     		RelativeLayout ll = (RelativeLayout) view.findViewById(R.id.card_rl); //取得personal_object中LinearLayout     
      		TextView cardid_text = (TextView)ll.findViewById(R.id.cardidtext);
      		TextView cardvalue_text = (TextView)ll.findViewById(R.id.cardvaluetext);
      		cardid_text.setText(cardID);
      		
      		CardIfonData cid=WebService.getCardcurrentPoint(cardID);
      	//	currentPoint=currentPoint.replace("$", "");
      		if(cid.currentPoint==-1)//沒錢或是卡號有問題
      		{
      			cardvalue_text.setText("");
      		}
      		else
      		{
      			cardvalue_text.setText(String.valueOf(cid.currentPoint));
      		}
      		
      		
			return view;
		}


		//加入庫存金的彈跳選單
		@SuppressLint("InflateParams")
		public Dialog onManualnputDialog() 
		{
		    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
		    // Get the layout inflater
		    LayoutInflater inflater = MainActivity.this.getLayoutInflater();
		    final View dialoglayout = inflater.inflate(R.layout.manualcardinput_layout, null);
		    // Inflate and set the layout for the dialog
		    // Pass null as the parent view because its going in the dialog layout
		    builder.setTitle("存入庫存金或轉成QRcode分享出去");
		    builder.setView(dialoglayout)
		    // Add action buttons
		           .setPositiveButton("存入庫存金", new DialogInterface.OnClickListener() {
		               @Override
		               public void onClick(DialogInterface dialog, int id) {	
		            	   final EditText  CardID = (EditText )(dialoglayout.findViewById(R.id.cardedittext));
		            	   new AsyncTask<String, Void, CardIfonData>() 
		            	   {

								@Override
								protected CardIfonData doInBackground(String... params) {
								 	CardIfonData cid=WebService.getCardcurrentPoint(params[0]);
									//String currentPoint=WebService.getCardcurrentPoint(params[0]);
									
									return cid;
								}
								 protected void onPostExecute(CardIfonData result)
								 {
									 	if(result.currentPoint==-1)//沒錢或是卡號有問題
					              		{
					            			Toast.makeText(MainActivity.this, "卡號："+CardID.getText().toString()+"請在檢查一遍", Toast.LENGTH_LONG).show();
					              		}
					            		else
					            		{
					            			dbHelper.createCard(CardID.getText().toString());
					            			Toast.makeText(MainActivity.this, "已將卡號："+CardID.getText().toString()+"存為你的庫存金", Toast.LENGTH_LONG).show();
					            		}
									 
									  super.onPostExecute(result);
								 }
		            	   }.execute(CardID.getText().toString());
		               }
		           })
		           .setNeutralButton("QRcode分享",new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		            	   final EditText  CardID = (EditText )(dialoglayout.findViewById(R.id.cardedittext));
		            	   
		                   //LoginDialogFragment.this.getDialog().cancel();
		            	   new AsyncTask<String, Void, CardIfonData>() 
		            	   {

								@Override
								protected CardIfonData doInBackground(String... params) {
								 	CardIfonData cid=WebService.getCardcurrentPoint(params[0]);
									//String currentPoint=WebService.getCardcurrentPoint(params[0]);
									
									return cid;
								}
								 protected void onPostExecute(CardIfonData result)
								 {
									    if(result.currentPoint==-1)//沒錢或是卡號有問題
					              		{
					            			Toast.makeText(MainActivity.this, "卡號："+CardID.getText().toString()+"請在檢查一遍", Toast.LENGTH_LONG).show();
					            			Log.e("QRcode", result.status);
					              		}
					            		else
					            		{
					            			//dbHelper.createCard(CardID.getText().toString());
					            			encodetoqr(0,"","","",CardID.getText().toString(),"", "",BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher),2); 
					            			//	Toast.makeText(MainActivity.this, "已將卡號："+CardID.getText().toString()+"存為你的庫存金", Toast.LENGTH_LONG).show();
					            		}
									 
									  super.onPostExecute(result);
								 }
		            	   }.execute(CardID.getText().toString());
		               }
		           })      
		           .setNegativeButton("取消", new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		                   //LoginDialogFragment.this.getDialog().cancel();
		               }
		           });      
		    return builder.create();
		}
		
		//子資料夾
		@SuppressLint("InflateParams")
		public Dialog onSubDocmentDialog(final int iconid) 
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			LayoutInflater inflater = MainActivity.this.getLayoutInflater();
			subiconviewlist = new ArrayList<HashMap<String,Object>>();
			 
			 
			final View dialoglayout = inflater.inflate(R.layout.subbocument_layout, null);
			final ProgressBar subprogressbar= (ProgressBar)dialoglayout. findViewById(R.id.SubprogressBar);


			new AsyncTask<Void, Void, Void>() 
			{

				@Override
					protected Void doInBackground(Void... params) {
						// TODO Auto-generated method stub
						subprogressbar.setVisibility(View.VISIBLE);
						Cursor c=dbHelper.getIcon( iconid);	//抓取該項目在主業面的顯示
						final String docname = c.getString(6);
						SubName=(TextView) dialoglayout.findViewById(R.id.SubName);
						SubName.setText(docname);
						SubRenamebtn=(ImageView) dialoglayout.findViewById(R.id.SubRename);
						SubRenamebtn.setOnClickListener(new OnClickListener(){

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
								    AlertDialog.Builder b = new AlertDialog.Builder(MainActivity.this);
								    // Get the layout inflater
								    LayoutInflater i = MainActivity.this.getLayoutInflater();
								    final View d = i.inflate(R.layout.rename_layout, null);
								    // Inflate and set the layout for the dialog
								    // Pass null as the parent view because its going in the dialog layout
								    final EditText  Renametext = (EditText )(d.findViewById(R.id.renameedittext));
								    Renametext.setText(docname);
								    Renametext.selectAll();
								    b.setTitle("重新命名");
								    b.setView(d)
								    // Add action buttons
								           .setPositiveButton("完成", new DialogInterface.OnClickListener() {
								               @Override
								               public void onClick(DialogInterface dialog, int id) {	
								            	
								            	   String text= Renametext.getText().toString();
								            	   SubName.setText(text);
								            	   
								            	   dbHelper.renewIconName(iconid, text);
								            	   if(dbHelper.getIcon(iconid).getInt(8)==-1)
								            	   {
									            	   int cid=dbHelper.getIcon(iconid).getInt(9);
									            	   HashMap<String, Object> temp = iconviewlist.get(cid);
									            	   temp.put("name",text);
									            	   iconviewlist.set(cid, temp);
									            	   mainadapter.notifyDataSetChanged();  
								            	   }
								            	   
								               }})
								    .setNegativeButton("取消", new DialogInterface.OnClickListener() {
							               public void onClick(DialogInterface dialog, int id) {
							                   //LoginDialogFragment.this.getDialog().cancel();
							               }
							           });      
								    b.create().show();
								}
							});
						c.close();
						c=dbHelper.getIconbypostion(iconid);
						int r = c.getCount();
						c.close();
						if(r!=0)
						{
							for(int i= 0;i<r;i++)
							{
								c=dbHelper.getIcon( iconid,i);
								
								int id = c.getInt(1);	//取得第0欄的資料，根據欄位type使用適當語法
								String url = c.getString(2);
								String card = c.getString(3);
								String to = c.getString(4);
								String from = c.getString(5);
								String name = c.getString(6);
								String icon = c.getString(7);
								int position = c.getInt(8);	
								int sort =c.getInt(9);
								Icondata t =new Icondata(id,url,card,to,from,name,icon,position,sort);
								Log.i("id", String.valueOf(t.id));
								Log.i("url", t.url);
								Log.i("card", t.card);
								Log.i("to", t.to);
								Log.i("from", t.from);
								Log.i("name", t.name);
								Log.i("icon", t.icon);
								Log.i("============", "=============");
								
							  	c=dbHelper.getIconbypostion(id);	//抓取該項目在主業面的顯示
						    	int s = c.getCount();	
						    	Log.i("dbHelper+", String.valueOf(s));
						    	
						    	c.close();
						    	if( s==0)
						    	{
									Bitmap iconbitmap =CommonUtilities.getBitmapFromURL("http://"+t.icon,MainActivity.this);
									CardIfonData cardIfon= WebService.getCardcurrentPoint(t.card);
									HashMap<String,Object> item = new HashMap<String,Object>();
									item.put("icon",iconbitmap);
									if(cardIfon.currentPoint>-1)
										item.put("cardvalue",String.valueOf(cardIfon.currentPoint));
									else
										item.put("cardvalue","");
									item.put("name",t.name);
									subiconviewlist.add(item);
						    	}
						    	else if(s>0)
						    	{
									Bitmap iconbitmap =BitmapFactory.decodeResource(getResources(), R.drawable.subdocm);
									if(s>=4)
									{
										Cursor c0=dbHelper.getIcon( id,0);
										Bitmap iconbitmap0 =CommonUtilities.getBitmapFromURL("http://"+c0.getString(7),MainActivity.this);
										c0.close();
										Cursor c1=dbHelper.getIcon( id,1);
										Bitmap iconbitmap1 =CommonUtilities.getBitmapFromURL("http://"+c1.getString(7),MainActivity.this);
										c1.close();
										Cursor c2=dbHelper.getIcon( id,2);
										Bitmap iconbitmap2 =CommonUtilities.getBitmapFromURL("http://"+c2.getString(7),MainActivity.this);
										c2.close();
										Cursor c3=dbHelper.getIcon( id,3);
										Bitmap iconbitmap3 =CommonUtilities.getBitmapFromURL("http://"+c3.getString(7),MainActivity.this);
										c3.close();
										iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
										iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.05);
										iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap2,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.55);
										iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap3,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.55);
										
									}
									else if (s==3)
									{
										Cursor c0=dbHelper.getIcon( id,0);
										Bitmap iconbitmap0 =CommonUtilities.getBitmapFromURL("http://"+c0.getString(7),MainActivity.this);
										c0.close();
										Cursor c1=dbHelper.getIcon( id,1);
										Bitmap iconbitmap1 =CommonUtilities.getBitmapFromURL("http://"+c1.getString(7),MainActivity.this);
										c1.close();
										Cursor c2=dbHelper.getIcon( id,2);
										Bitmap iconbitmap2 =CommonUtilities.getBitmapFromURL("http://"+c2.getString(7),MainActivity.this);
										c2.close();
										iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
										iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.05);
										iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap2,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.55);
									}
									else if (s==2)
									{
										Cursor c0=dbHelper.getIcon( id,0);
										Bitmap iconbitmap0 =CommonUtilities.getBitmapFromURL("http://"+c0.getString(7),MainActivity.this);
										c0.close();
										Cursor c1=dbHelper.getIcon( id,1);
										Bitmap iconbitmap1 =CommonUtilities.getBitmapFromURL("http://"+c1.getString(7),MainActivity.this);
										c1.close();
										iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
										iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.05);
										
									}
									else if (s==1)
									{
										Cursor c0=dbHelper.getIcon( id,0);
										Bitmap iconbitmap0 =CommonUtilities.getBitmapFromURL("http://"+c0.getString(7),MainActivity.this);
										c0.close();
										iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
									}
									HashMap<String,Object> item = new HashMap<String,Object>();
									item.put("icon",iconbitmap);
									item.put("cardvalue","");
									item.put("name",t.name);
									subiconviewlist.add(item);
						    	}
						    	else
						    	{
						    		 Toast.makeText(MainActivity.this, "資料庫出現錯誤", Toast.LENGTH_LONG).show();
						    	}
							}
						}
						
						return null;
					}
					protected void onPostExecute(Void result)
					{
							Subadapter=new SimpleAdapter(MainActivity.this, 
									subiconviewlist, R.layout.icon_layout, new String[]{"icon", "cardvalue","name"},
								    new int[]{R.id.icon, R.id.cardvalue,R.id.name});
							Subadapter.setViewBinder(new ViewBinder() {

						        @Override
						        public boolean setViewValue(View view, Object data,
						            String textRepresentation) {
						          if (view instanceof ImageView && data instanceof Bitmap) {
						            ImageView iv = (ImageView) view;
						        	Bitmap scaledBitmap= Bitmap.createScaledBitmap((Bitmap) data, 192, 192, true);
						            iv.setImageBitmap(scaledBitmap);
						            return true;
						          }
						          if (view instanceof TextView && data instanceof String) {
						        	  TextView tv = (TextView) view;
						        	  if(String.valueOf(data)=="")
						        	  {
						        		  tv.setVisibility(View.INVISIBLE);
						        	  }
						        	  else
						        	  {
						        		  tv.setVisibility(View.VISIBLE);
						        		  tv.setText((CharSequence) data);
						        	  }
							            return true;
							          }
						          return false;
						        }
						      });
							SubdragGridView=(DragGridView) dialoglayout.findViewById(R.id.SubdragGridView);
							SubdragGridView.setAdapter(Subadapter);
							SubdragGridView.setOnItemClickListener(new OnItemClickListener(){

								@Override
								public void onItemClick(AdapterView<?> parent,
										View view, int position, long id) {
									// TODO Auto-generated method stub
									Cursor  c = dbHelper.getIcon(iconid,position);	//取得SQLite類別的回傳值:Cursor物件
									int cid = c.getInt(1);
							    	Log.i("cid", String.valueOf(cid));
							     	Cursor cursor = dbHelper.getIconbypostion(cid);	//取得SQLite類別的回傳值:Cursor物件
							    	int rows_num = cursor.getCount();
							     	if(rows_num == 0)
					 			  	{
										int cardid = c.getInt(1);	//取得第0欄的資料，根據欄位type使用適當語法
										String url = c.getString(2);
										String card = c.getString(3);
										String to = c.getString(4);
										String from = c.getString(5);
										String name = c.getString(6);
										String icon = c.getString(7);
										int iconposition = c.getInt(8);	
										int iconsort = c.getInt(9);	
										Bitmap iconbitmap =(Bitmap) subiconviewlist.get(position).get("icon");
									    showiconmeun(cid,cardid,url,name,icon,card,to,from,iconposition,iconsort,iconbitmap);
					 			  	}
							     	else if(rows_num>=1) //資料夾
							     	{
					 			  	    if(SubAlert!=null)
										{
											if(SubAlert.isShowing())	
												SubAlert.dismiss();
											SubAlert=null;
										}
							     		SubAlert=(AlertDialog) onSubDocmentDialog(cid);
							
							     		SubAlert.show();
							     		//Toast.makeText(MainActivity.this, "你按下第"+position+"個", Toast.LENGTH_LONG).show();
							     	}
							     	else
							     	{
							     		 Toast.makeText(MainActivity.this, "資料庫出現錯誤", Toast.LENGTH_LONG).show();
							     	}
							     	cursor.close();
							     	c.close();
							     	
								}});
							SubdragGridView.setOnChangeListener(new OnChanageListener()
							{
								 public void onChange(int from, int to) {  
						                HashMap<String, Object> temp = subiconviewlist.get(from);  

						                  
						                if(from < to){  
						                    for(int i=from; i<to; i++){  
						                        Collections.swap(subiconviewlist, i, i+1);  
						                        dbHelper.renewIconPostion(iconid,i,iconid,-2); 
						                        dbHelper.renewIconPostion(iconid,i+1,iconid, i); 
						                        dbHelper.renewIconPostion(iconid,-2,iconid, i+1);
						                    }  
						                }else if(from > to){  
						                    for(int i=from; i>to; i--){  
						                        Collections.swap(subiconviewlist, i, i-1);  
						                        dbHelper.renewIconPostion(iconid,i,iconid,-2); //a=t
						                        dbHelper.renewIconPostion(iconid,i-1,iconid, i); 
						                        dbHelper.renewIconPostion(iconid,-2,iconid, i-1);
						                    }  
						                }  
						                  
						                subiconviewlist.set(to, temp);  
						                  
						                Subadapter.notifyDataSetChanged();  
						                  
						                  
						            }  
							});
							SubdragGridView.setOnOutsideListener(new OnOutsideListener(){

								@Override
								public void OnOutside(int id) {
									// TODO Auto-generated method stub
									 //Toast.makeText(MainActivity.this, "OnOutside", Toast.LENGTH_LONG).show();
								//	Toast.makeText(MainActivity.this, "OnOutside", Toast.LENGTH_LONG).show();
									Cursor  c = dbHelper.getIcon(iconid);	//取得SQLite類別的回傳值:Cursor物件
									int cpostion = c.getInt(8);
									c.close();
									if(cpostion==-1)
									{
										icon_num++;
					 			  		settings.edit()
					 					.putInt("icon_num", icon_num)
					 					.commit();
					 			  		dbHelper.renewIconPostion(iconid,id,cpostion,icon_num); //移到主業面
					 			  		Log.i("dbHelper.renewIconPostion(iconid,id,cpostion,icon_num);", "dbHelper.renewIconPostion("+String.valueOf(iconid)+","+ String.valueOf(id)+","+ String.valueOf(cpostion)+", "+ String.valueOf(id)+");");
									}
									else
									{
										int sort=dbHelper.getIconbypostion(cpostion).getCount();
										dbHelper.renewIconPostion(iconid,id,cpostion,sort); //移到主業面
										Log.i("dbHelper.renewIconPostion(iconid,id,cpostion,sort);", "dbHelper.renewIconPostion("+String.valueOf(iconid)+","+ String.valueOf(id)+","+ String.valueOf(cpostion)+", "+ String.valueOf(sort)+");");
									}
				 			  		for(int i =id;i<subiconviewlist.size();i++)
									{
				 			  		  dbHelper.renewIconPostion(iconid,i+1,iconid, i); 
				 			  		Log.i("1341", "dbHelper.renewIconPostion("+String.valueOf(iconid)+","+ String.valueOf(i+1)+","+ String.valueOf(iconid)+", "+ String.valueOf(i)+");");
									}
				 			  		HashMap<String, Object> temp = subiconviewlist.get(id);
				 			  		if(cpostion==-1)
				 			  		{
				 			  			iconviewlist.add(temp);
				 			  		}
				 			  		subiconviewlist.remove(id);
				 			  		int cid=dbHelper.getIcon(iconid).getInt(9);
				 			  		if(subiconviewlist.size()==4)
									{
										if(cpostion==-1)
										{
					 			  			HashMap<String, Object> maintemp = iconviewlist.get(cid);//主頁面資料夾要重作
						 			  		Bitmap iconbitmap =BitmapFactory.decodeResource(getResources(), R.drawable.subdocm);
						 			  		maintemp.put("icon", iconbitmap);
						 			  		
											Bitmap iconbitmap0 =(Bitmap) subiconviewlist.get(0).get("icon");
											Bitmap iconbitmap1 =(Bitmap) subiconviewlist.get(1).get("icon");
											Bitmap iconbitmap2 =(Bitmap) subiconviewlist.get(2).get("icon");
											Bitmap iconbitmap3 =(Bitmap) subiconviewlist.get(3).get("icon");
											
											iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
											iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.05);
											iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap2,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.55);
											iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap3,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.55);
											maintemp.put("icon", iconbitmap);
	
											iconviewlist.set(cid, maintemp);
										}
									}
									else if (subiconviewlist.size()==3)
									{
										if(cpostion==-1)
										{
											HashMap<String, Object> maintemp = iconviewlist.get(cid);//主頁面資料夾要重作
						 			  		Bitmap iconbitmap =BitmapFactory.decodeResource(getResources(), R.drawable.subdocm);
						 			  		maintemp.put("icon", iconbitmap);
						 			  		
											Bitmap iconbitmap0 =(Bitmap) subiconviewlist.get(0).get("icon");
											Bitmap iconbitmap1 =(Bitmap) subiconviewlist.get(1).get("icon");
											Bitmap iconbitmap2 =(Bitmap) subiconviewlist.get(2).get("icon");
											
											iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
											iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.05);
											iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap2,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.55);
										
											maintemp.put("icon", iconbitmap);

											iconviewlist.set(cid, maintemp);
										}
									}
									else if (subiconviewlist.size()==2)
									{
										if(cpostion==-1)
										{
											HashMap<String, Object> maintemp = iconviewlist.get(cid);//主頁面資料夾要重作
						 			  		Bitmap iconbitmap =BitmapFactory.decodeResource(getResources(), R.drawable.subdocm);
						 			  		maintemp.put("icon", iconbitmap);
						 			  		
											Bitmap iconbitmap0 =(Bitmap) subiconviewlist.get(0).get("icon");
											Bitmap iconbitmap1 =(Bitmap) subiconviewlist.get(1).get("icon");
											
											iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
											iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap1,  CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.55,(float) 0.05);
											maintemp.put("icon", iconbitmap);
											iconviewlist.set(cid, maintemp);
										}
										
									}
									else if (subiconviewlist.size()==1)
									{
										if(cpostion==-1)
										{
											HashMap<String, Object> maintemp = iconviewlist.get(cid);//主頁面資料夾要重作
						 			  		Bitmap iconbitmap =BitmapFactory.decodeResource(getResources(), R.drawable.subdocm);
						 			  		maintemp.put("icon", iconbitmap);
						 			  		
											Bitmap iconbitmap0 =(Bitmap) subiconviewlist.get(0).get("icon");
											
											iconbitmap=CommonUtilities.combineBitmap(iconbitmap, iconbitmap0, CommonUtilities.ICON_SCALE, CommonUtilities.ICON_SCALE,(float) 0.05,(float) 0.05);
											maintemp.put("icon", iconbitmap);

											iconviewlist.set(cid, maintemp);
										}
									}
									else if(subiconviewlist.size()==0)//沒東西 直接砍掉資料夾
									{
										int sort=dbHelper.getIcon(iconid).getInt(9);
										if( dbHelper.deleteIcon(iconid))
										{
											if(cpostion==-1)
											{
											
												for(int i =id;i<icon_num;i++)
												{
													dbHelper.renewIconPostion(cpostion,i+1,cpostion, i);
													Log.i("dbHelper.renewIconPostion(cpostion,i+1,cpostion,i);", "dbHelper.renewIconPostion("+String.valueOf(cpostion)+","+ String.valueOf(i+1)+","+ String.valueOf(cpostion)+", "+ String.valueOf(i)+");");
												}

													icon_num--;
											  		settings.edit()
													.putInt("icon_num", icon_num)
													.commit();
											  		iconviewlist.remove(cid);
											}
											else
											{
												int all=dbHelper.getIconbypostion(cpostion).getCount();
												for(int i =sort;i<all;i++)
												{
													dbHelper.renewIconPostion(cpostion,i+1,cpostion, i);
													Log.i("dbHelper.renewIconPostion(cpostion,i+1,cpostion,i);", "dbHelper.renewIconPostion("+String.valueOf(cpostion)+","+ String.valueOf(i+1)+","+ String.valueOf(cpostion)+", "+ String.valueOf(i)+");");
												}
											}
										}
									}
				 			  		
				 			  		mainadapter.notifyDataSetChanged();  
				 			  	    Subadapter.notifyDataSetChanged();  
				 			  	    if(SubAlert!=null)
									{
										if(SubAlert.isShowing())	
											SubAlert.dismiss();
										SubAlert=null;
									}
								}
								
							});
							
							subprogressbar.setVisibility(View.GONE);
						 super.onPostExecute(result);
					}
					
				}.execute();
			
			
			builder.setView(dialoglayout);
			 return builder.create();
		}
		
         //對應其他需要輸入單一綁定電話 的選單
		 @SuppressLint({ "InflateParams", "SetJavaScriptEnabled" })
			public Dialog oninputDialog(String  title,String hint ,final int type,final int cid ) 
			{
				LayoutInflater inflater = MainActivity.this.getLayoutInflater();
				final View dialoglayout = inflater.inflate(R.layout.manualcardinput_layout, null);
			    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			    builder.setTitle(title);
			    final EditText  inputEditText = (EditText )(dialoglayout.findViewById(R.id.cardedittext));
			    inputEditText.setHint(hint);
			    builder.setView(dialoglayout)
			    .setPositiveButton("取消", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
					}
			    })
			       .setNeutralButton("確定",new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if(type==1)
						{
							dbHelper.renewIconToPhone(cid,inputEditText.getText().toString());
						}
						else if (type==2)
						{
							dbHelper.renewIconFormPhone(cid,inputEditText.getText().toString());
						}
						 Toast.makeText(MainActivity.this, "輸入電話" +inputEditText.getText().toString()+
						 		"完成", Toast.LENGTH_LONG).show();
					}
			    	   
			       });
			    
			
			    return builder.create();
			}
		 
		 
		 //輸入兩支電話綁定的選單
		 @SuppressLint({ "InflateParams", "SetJavaScriptEnabled" })
			public Dialog ondoubleinputDialog(final int cid ) 
			{
				LayoutInflater inflater = MainActivity.this.getLayoutInflater();
				final View dialoglayout = inflater.inflate(R.layout.doubleinputlayout, null);
			    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			    builder.setTitle("兩支電話都未綁定");
			    final EditText  inputEditText1 = (EditText )(dialoglayout.findViewById(R.id.editText1));
			    final EditText  inputEditText2 = (EditText )(dialoglayout.findViewById(R.id.editText2));
			    builder.setView(dialoglayout)
			    .setPositiveButton("取消", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
					}
			    })
			       .setNeutralButton("確定",new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
							dbHelper.renewIconToPhone(cid,inputEditText2.getText().toString());
							dbHelper.renewIconFormPhone(cid,inputEditText1.getText().toString());
			
					}
			    	   
			       });
			    
			
			    return builder.create();
			}
		 

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		dbHelper.close();
		super.onDestroy();
	}

}
