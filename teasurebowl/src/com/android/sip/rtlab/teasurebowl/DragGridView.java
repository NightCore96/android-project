package com.android.sip.rtlab.teasurebowl;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

//為了做出桌面拖拉效果的額外UI元件
//有合併 跟拖拉功能

public class DragGridView extends GridView{

	 	private long dragResponseMS = 1000;//長按回覆時間
	 
	 	private boolean isDrag = false;  //是否可以拖曳
	 	
	 	private boolean isCombine = false;  //是否可以拖曳
	 	
	    private int mDownX;  
	    private int mDownY;  
	    private int moveX;  
	    private int moveY;  
	    
	    
	    
	    private int mDragPosition;  //拖曳的那個位子
	    private int mMergePosition;  //拖曳的那個位子
	    
	    private View mStartDragItemView = null;  //被拖曳的view
	    
	    
	    private ImageView mDragImageView; //被拖曳之後那個透明view
	    private ImageView mMergeItemView;  //合併之後的
	    
	    private Vibrator mVibrator; //按下會震一下
	  //透明view如何布局
	    private WindowManager mWindowManager; 
	    private WindowManager.LayoutParams mWindowLayoutParamsDrag;  
	    private WindowManager.LayoutParams mWindowLayoutParamsMerge;   

	    private Bitmap mDragBitmap;//被拖那個張圖
	    private Bitmap mMergeBitmap;//合併前時需要示意圖
	    
	    //按下的點到所在Item的上邊距跟右邊距的距離
	    private int mPointItemTop ;
	    private int mPointItemLeft;
	    
	    //整個GridView2到螢幕的距離
	    private int mOffsetTop;
	    private int mOffsetLeft;
	    
	    //status的高度
	    private int mStatusHeight;
	    
	    //上下滾的邊界值
	    private int mDownScrollBorder;
	    private int mUpScrollBorder;
	    
	    //滾多快
	    private static final int speed = 20;
	 
	    //交換位子的界面
	    private OnChanageListener onChanageListener;  
	    private OnMergeListener onMergeListener;  
	    private OnOutsideListener onOutsideListener;  
	    
	    
	    public DragGridView(Context context) {
	    
		// TODO Auto-generated constructor stub
	    	this(context, null);
	}

		public DragGridView(Context context, AttributeSet attrs) {
			// TODO Auto-generated constructor stub
			this(context, attrs, 0);
		}
	
		 public DragGridView(Context context, AttributeSet attrs, int defStyle) {  
		        super(context, attrs, defStyle);  
		        mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);  
		        mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);  
		        mStatusHeight = getStatusHeight(context); 
		        onChanageListener=null;
		        onMergeListener=null;
		        onOutsideListener=null;
		        mMergeBitmap=null;
		    }  

		 private Handler mHandler = new Handler();
		 //處理長按之後Runnable
		 private Runnable mLongClickRunnable = new Runnable() {  
	          
		        @Override  
		        public void run() {  
		            isDrag = true;
		            mVibrator.vibrate(50);   
		            mStartDragItemView.setVisibility(View.INVISIBLE);
		             //顯示按下之後的影像
		            createDragImage(mDragBitmap, mDownX, mDownY);  
		        }  
		    };  
		    //設定
		    public void setOnChangeListener(OnChanageListener onChanageListener){  
		        this.onChanageListener = onChanageListener;  
		    }
		    public void setOnMergeListener(OnMergeListener onMergeListener){  
		        this.onMergeListener = onMergeListener;  
		    }
		    public void setOnOutsideListener(OnOutsideListener onOutsideListener){  
		        this.onOutsideListener = onOutsideListener;  
		    }
		  //設定
		    public void setDragResponseMS(long dragResponseMS) {  
		        this.dragResponseMS = dragResponseMS;  
		    } 
		    public void setMergeBG(Bitmap MergeBG) {  
		        this.mMergeBitmap = MergeBG;  
		    } 
		    
		    //處理對應按下動作
		    @Override  
		    public boolean dispatchTouchEvent(MotionEvent ev) {  
		        switch(ev.getAction()){  
		        case MotionEvent.ACTION_DOWN:  
		            mDownX = (int) ev.getX();  
		            mDownY = (int) ev.getY();  
		              
		         //抓到誰被拖曳
		            mDragPosition = pointToPosition(mDownX, mDownY);  
		              
		              
		            if(mDragPosition == AdapterView.INVALID_POSITION){  
		                return super.dispatchTouchEvent(ev);  
		            }  
		              
		            //執行mLongClickRunnable
		            mHandler.postDelayed(mLongClickRunnable, dragResponseMS);  
		              
		            //抓到現在的位子view
		            mStartDragItemView = getChildAt(mDragPosition - getFirstVisiblePosition());  
		              
		            
		            mPointItemTop = mDownY - mStartDragItemView.getTop();  
		            mPointItemLeft = mDownX - mStartDragItemView.getLeft();  
		              
		            mOffsetTop = (int) (ev.getRawY() - mDownY);  
		            mOffsetLeft = (int) (ev.getRawX() - mDownX);  
		              
		          
		            mDownScrollBorder = getHeight() /4;  
		         
		            mUpScrollBorder = getHeight() * 3/4;  
		              
		              
		              
		              
		            mStartDragItemView.setDrawingCacheEnabled(true);  
		            
		            mDragBitmap = Bitmap.createBitmap(mStartDragItemView.getDrawingCache());  
		              
		            mStartDragItemView.destroyDrawingCache();  
		              
		              
		            break;  
		        case MotionEvent.ACTION_MOVE:  
		            int moveX = (int)ev.getX();  
		            int moveY = (int) ev.getY();  
		              
		         
		            if(!isTouchInItem(mStartDragItemView, moveX, moveY)){  
		                mHandler.removeCallbacks(mLongClickRunnable);  
		            }  
		            break;  
		        case MotionEvent.ACTION_UP:  
		            mHandler.removeCallbacks(mLongClickRunnable);  
		            mHandler.removeCallbacks(mScrollRunnable);  
		            break;  
		        }  
		        return super.dispatchTouchEvent(ev);  
		    }  
		    
		    private boolean isTouchInItem(View dragView, int x, int y){  
		        if(dragView == null){  
		            return false;  
		        }  
		        int leftOffset = dragView.getLeft();  
		        int topOffset = dragView.getTop();  
		        if(x < leftOffset || x > leftOffset + dragView.getWidth()){  
		            return false;  
		        }  
		          
		        if(y < topOffset || y > topOffset + dragView.getHeight()){  
		            return false;  
		        }  
		          
		        return true;  
		    }
		    
		    @SuppressLint("ClickableViewAccessibility")
			@Override  
		    public boolean onTouchEvent(MotionEvent ev) {  
		        if(isDrag && mDragImageView != null){  
		            switch(ev.getAction()){  
		            case MotionEvent.ACTION_MOVE:  
		                moveX = (int) ev.getX();  
		                moveY = (int) ev.getY();    
		                onDragItem(moveX, moveY);  
		                break;  
		            case MotionEvent.ACTION_UP:  
		            	Log.i("ev.getX()", String.valueOf(ev.getX()));
		            	Log.i("ev.getY()", String.valueOf(ev.getY()));
		            	Log.i("getLeft", String.valueOf(getLeft()));
		            	Log.i("getRight", String.valueOf(getRight()));
		            	Log.i("getTop", String.valueOf(getTop()));
		            	Log.i("getBottom", String.valueOf(getBottom()));
				        if(mMergeItemView!=null&&onMergeListener != null)
				        {
				        	onMergeListener.OnMerge(mDragPosition, mMergePosition);
			                onStopDrag();  
			                isDrag = false;  
			                break; 
				        }
				        
		            	if((ev.getY()<this.getTop()))
		            	{
		            		if(onOutsideListener!=null)
		            		{
		            			onOutsideListener.OnOutside(mDragPosition);
				                onStopDrag();  
				                isDrag = false;  
				                break; 
		            		}
		            	}
		                onStopDrag();  
		                isDrag = false;  
		                break;  
		            }  
		            return true;  
		        }  
		        return super.onTouchEvent(ev);  
		    }
		    
		    private void createDragImage(Bitmap bitmap, int downX , int downY){  
		        mWindowLayoutParamsDrag = new WindowManager.LayoutParams();  
		        mWindowLayoutParamsDrag.format = PixelFormat.TRANSLUCENT; //圖以外其他透明
		        mWindowLayoutParamsDrag.gravity = Gravity.TOP | Gravity.LEFT;  
		        mWindowLayoutParamsDrag.x = downX - mPointItemLeft + mOffsetLeft;  
		        mWindowLayoutParamsDrag.y = downY - mPointItemTop + mOffsetTop - mStatusHeight;  
		        mWindowLayoutParamsDrag.alpha = 0.55f; //透明度  
		        mWindowLayoutParamsDrag.width = WindowManager.LayoutParams.WRAP_CONTENT;    
		        mWindowLayoutParamsDrag.height = WindowManager.LayoutParams.WRAP_CONTENT;    
		        mWindowLayoutParamsDrag.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE    
		                    | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ;  
		            
		        mDragImageView = new ImageView(getContext());    
		        mDragImageView.setImageBitmap(bitmap);    
		        mWindowManager.addView(mDragImageView, mWindowLayoutParamsDrag);    
		    }
		    
		    private void createMergeImage(Bitmap bitmap, int downX , int downY)
		    {
		        mWindowLayoutParamsMerge = new WindowManager.LayoutParams();  
		        mWindowLayoutParamsMerge.format = PixelFormat.TRANSLUCENT; //圖以外其他透明
		        mWindowLayoutParamsMerge.gravity = Gravity.TOP | Gravity.LEFT;  
		        mWindowLayoutParamsMerge.x = downX + mOffsetLeft;  
		        mWindowLayoutParamsMerge.y = downY + mOffsetTop - mStatusHeight;  
		        mWindowLayoutParamsMerge.width = WindowManager.LayoutParams.WRAP_CONTENT;    
		        mWindowLayoutParamsMerge.height = WindowManager.LayoutParams.WRAP_CONTENT;    
		        mWindowLayoutParamsMerge.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE    
		                    | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ;  
		            
		        mMergeItemView = new ImageView(getContext());    
		        mMergeItemView.setImageBitmap(bitmap);    
		        mWindowManager.addView(mMergeItemView, mWindowLayoutParamsMerge);    
		    }
		    
		    private void removeDragImage(){  
		        if(mDragImageView != null){  
		            mWindowManager.removeView(mDragImageView);  
		            mDragImageView = null;  
		        }  
		    }
		    private void removeMergeImage(){  
		        if(mMergeItemView != null){  
		            mWindowManager.removeView(mMergeItemView);  
		            mMergeItemView = null;  
		        }  
		    }
		    
		    private void onDragItem(int moveX, int moveY){  
		        mWindowLayoutParamsDrag.x = moveX - mPointItemLeft + mOffsetLeft;  
		        mWindowLayoutParamsDrag.y = moveY - mPointItemTop + mOffsetTop - mStatusHeight;  
		        mWindowManager.updateViewLayout(mDragImageView, mWindowLayoutParamsDrag); //更新位置  
		        onMoveItem(moveX, moveY);  
		          
		        //GridView自己滾  
		        mHandler.post(mScrollRunnable);  
		    }
		    
		    private Runnable mScrollRunnable = new Runnable() {  
		          
		        @Override  
		        public void run() {  
		            int scrollY;  
		            if(moveY > mUpScrollBorder){  
		                 scrollY = speed;  
		                 mHandler.postDelayed(mScrollRunnable, 25);  
		            }else if(moveY < mDownScrollBorder){  
		                scrollY = -speed;  
		                 mHandler.postDelayed(mScrollRunnable, 25);  
		            }else{  
		                scrollY = 0;  
		                mHandler.removeCallbacks(mScrollRunnable);  
		            }  
		              
		            onMoveItem(moveX, moveY);  
		              
		              
		            smoothScrollBy(scrollY, 10);  
		        }  
		    };
		    //是否要執行合併動作
		    private boolean isMerge(int moveX, int moveY,int tempPosition)
		    {
		    	Log.i("isMerge", "========================== ");
		    	Log.i("isMerge", "moveX "+moveX);
		    	Log.i("isMerge", "tempPosition "+ tempPosition);
		    	int tempViewWidth=getChildAt(tempPosition - getFirstVisiblePosition()).getWidth();
		    	Log.i("isMerge", "tempViewWidth "+tempViewWidth);
		    	int tempViewLeft=getChildAt(tempPosition - getFirstVisiblePosition()).getLeft();
		    	Log.i("isMerge", "tempViewLeft "+ tempViewLeft);
		    	int tempViewTop=getChildAt(tempPosition - getFirstVisiblePosition()).getTop();
		    	int tempViewRight=getChildAt(tempPosition - getFirstVisiblePosition()).getRight();
		    	int tempViewBottom=getChildAt(tempPosition - getFirstVisiblePosition()).getBottom();
		    	getChildAt(tempPosition - getFirstVisiblePosition()).getHeight();
		    	if(mDragPosition<tempPosition)
		    	{
		    		if((Math.abs(moveX-tempViewLeft)<tempViewWidth/3)&&(Math.abs(moveY-tempViewTop)!=0))
		    		{
		    			Log.i("isMergetrue", "Math.abs(moveX-tempViewLeft)<tempViewWidth/4 ");
		    			return true;
		    		}
		    		Log.i("isMergefalse", "Math.abs(moveX-tempViewLeft)<tempViewWidth/4 ");
		    		return false;
		    	}
		    	else
		    	{
		    		if(Math.abs(moveX-tempViewLeft)>tempViewWidth*2/3&&(Math.abs(moveY-tempViewTop)!=0))
		    		{
		    			Log.i("isMergetrue", "Math.abs(moveX-tempViewLeft)>tempViewWidth*3/4 ");
		    			return true;
		    		}
		    		Log.i("isMergefalse", "Math.abs(moveX-tempViewLeft)>tempViewWidth*3/4 ");
		    		return false;
		    	}
		    }
		    
		    @SuppressLint("NewApi")
			private void onMoveItem(int moveX, int moveY){  
		        //抓取位置
		        int tempPosition = pointToPosition(moveX, moveY);  
		        if( mMergePosition != tempPosition)
		        {
		        	removeMergeImage();
		        }
		          
		        //
		        if(tempPosition != mDragPosition && tempPosition != AdapterView.INVALID_POSITION){  
		        	 boolean isMergeitem=isMerge( moveX,  moveY, tempPosition);	
		        	 boolean isChangeStauts=false;
		        	 if(	 isCombine!=isMergeitem)
		        	 {
		        		 isChangeStauts=true;
		        	 }
		        	
		        	 isCombine=isMergeitem;
		        	 if(isMergeitem)
		        	 {
		        		 if(isChangeStauts&&mMergeBitmap!=null)
		        		 {
		        				int X=getChildAt(tempPosition - getFirstVisiblePosition()).getLeft();
		        		    	int Y=getChildAt(tempPosition - getFirstVisiblePosition()).getTop();
		        		    	createMergeImage(mMergeBitmap,X,Y);
		        		 }
		        		 mMergePosition = tempPosition;  
		        	 }
		        	 else
		        	 {
		        		 removeMergeImage();
		        		 if(onChanageListener != null)
		        		 {  
				                onChanageListener.onChange(mDragPosition, tempPosition);  
				            }  
				              
				            getChildAt(tempPosition - getFirstVisiblePosition()).setVisibility(View.INVISIBLE);  
				            getChildAt(mDragPosition - getFirstVisiblePosition()).setVisibility(View.VISIBLE); 
				              
				            mDragPosition = tempPosition;  
		        	 }
		            
		        }  
		        
		     }  
		    
		    private void onStopDrag(){  
		        View view = getChildAt(mDragPosition - getFirstVisiblePosition());  
		        if(view != null){  
		            view.setVisibility(View.VISIBLE);  
		        }  
		     //   ((DragAdapter)this.getAdapter()).setItemHide(-1);  
		        removeMergeImage();
		        removeDragImage();  
		       
		    }
		    
		    private static int getStatusHeight(Context context){  
		        int statusHeight = 0;  
		        Rect localRect = new Rect();  
		        ((Activity) context).getWindow().getDecorView().getWindowVisibleDisplayFrame(localRect);  
		        statusHeight = localRect.top;  
		        if (0 == statusHeight){  
		            Class<?> localClass;  
		            try {  
		                localClass = Class.forName("com.android.internal.R$dimen");  
		                Object localObject = localClass.newInstance();  
		                int i5 = Integer.parseInt(localClass.getField("status_bar_height").get(localObject).toString());  
		                statusHeight = context.getResources().getDimensionPixelSize(i5);  
		            } catch (Exception e) {  
		                e.printStackTrace();  
		            }   
		        }  
		        return statusHeight;  
		    }
		    
		    public interface OnChanageListener{  
		          
		        public void onChange(int form, int to);  
		    } 
			public interface OnMergeListener{  
			    
				public void OnMerge(int form, int to);  
			} 
			public interface OnOutsideListener{  
			    
				public void OnOutside(int id);  
			} 
}
		