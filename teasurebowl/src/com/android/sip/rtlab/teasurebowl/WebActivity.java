package com.android.sip.rtlab.teasurebowl;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

//連上網路呼叫打電話的API

public class WebActivity  extends Activity{
	
	private TbowlDB dbHelper;//通訊錄
	
	
	@SuppressLint({ "SetJavaScriptEnabled", "JavascriptInterface" })
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.web_layout);
		
		//通訊錄DB
		dbHelper = new TbowlDB(this);
		
		String loadURL="https://www.google.com.tw";
		String loadName="對方";
		Bundle bundle = this.getIntent().getExtras();
		if(bundle!=null)
		{
		 loadURL = bundle.getString("URL");
		 loadName = bundle.getString("NAME");
		}
		  Uri phoneURI = this.getIntent().getData(); 
		  if (phoneURI != null)
		  {
		 		loadURL =phoneURI.toString();
		  }
		final String ShowName=loadName;
		  final TextView contentView = (TextView) findViewById(R.id.contentView);
		 
		  final ProgressDialog d=new ProgressDialog(WebActivity.this);

		  	d.setTitle("雲端接線生接通"+ShowName+"中");
            d.setMessage("請稍等...");
            d.show();   
            
  		 new  AsyncTask<String, Void, String>()
  		 {

  			@Override
  			protected String doInBackground(String... params) {
	
  				 GetServerMessage message = new GetServerMessage();
  				  String url = params[0];
  				  Uri u=Uri.parse(url);
  				  String cValue = u.getQueryParameter("ac");
  				
  				  CardIfonData cid=WebService.getCardcurrentPoint(cValue);
  				  if(cid.currentPoint<=0)//沒錢或是卡號有問題
  				  {	
  					Log.i("WebActivity", "沒錢拉");
  					   Cursor cursor = dbHelper.getAllCard();	//取得SQLite類別的回傳值:Cursor物件
  						int rows_num = cursor.getCount();	//取得資料表列數
  						if(rows_num != 0) 
  						{
  							cursor.moveToFirst();			//將指標移至第一筆資料
  							for(int i=0; i<rows_num; i++) 
  							{
  								String cardID = cursor.getString(1);
  								CardIfonData c=WebService.getCardcurrentPoint(cardID);
  							  if(c.currentPoint<=0)//沒錢或是卡號有問題
  							  {
  								Log.i("WebActivity", "卡號"+cardID+"沒錢 "+c.currentPoint);
  								//dbHelper.deleteCard(cardID);
  							  }
  							  else
  							  {
  								Log.i("WebActivity", "卡號"+cardID+"有錢 "+c.currentPoint);
  								url=url.replace(cValue, cardID);
  							//	 Toast.makeText(WebActivity.this, "使用庫存金 卡號: "+cardID, Toast.LENGTH_LONG).show();
  								break;
  							  }
  							}
  						}
  						else
  						{
  							return "請檢查你的卡號: " +cValue+" 是否正確或是還有餘額?";
  						}
  				  }
  				  
  				  
  				  String msg = message.stringQuery(url);
  				  Log.i("WebActivity", url);
  				 
  	//			Toast.makeText(WebActivity.this, "Link to : "+url, Toast.LENGTH_LONG).show();
  				 
  				 
  				  return msg;
  			}
  			 protected void onPostExecute(String result) 
  	          {
  			     d.dismiss();
  				 super.onPostExecute(result);
  				 if(result.indexOf("SUCCESS")<0)
  				 {
  					 contentView.setText("雲端接線生接通發生錯誤，錯誤訊息如下：\n"+result);
  					 
  				 }
  				 else
  				 {
  					 contentView.setText("雲端接線生接通成功"); 
  					String Name=ShowName;
  					
  					final Toast tag = Toast.makeText(WebActivity.this, "雲端接線生接通 "+ Name +" 中，稍後雲端接線生會來電，請放心接聽", Toast.LENGTH_SHORT);
  					tag.show();

  					new CountDownTimer(9000, 1000)
  					{

  					    public void onTick(long millisUntilFinished) {tag.show();}
  					    public void onFinish() {tag.show();}

  					}.start();
  					 
  					// WebActivity.this.finish();
  				 }
  	          } 
  			}.execute(loadURL);
	}
	
	public class GetServerMessage {

	    public String stringQuery(String url){
	        try
	        {
	            HttpClient httpclient = new DefaultHttpClient();
	            HttpPost method = new HttpPost(url);
	            HttpResponse response = httpclient.execute(method);
	            HttpEntity entity = response.getEntity();
	            if(entity != null){
	                return EntityUtils.toString(entity);
	            }
	            else{
	                return "No response.";
	            }
	         }
	         catch(Exception e){
	             return "Network problem";
	         }
	    }
	}
	   
}
