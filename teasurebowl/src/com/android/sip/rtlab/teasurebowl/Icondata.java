package com.android.sip.rtlab.teasurebowl;

//一個ICON 的基本資料

public class Icondata {
	
	public int id ;
	public String url ;
	public String name;
	public  String icon ;
	public  String card ;
	public String from ;
	public String to ;     
	public int position ;
	public int sort ;
    public Icondata(int arg0,String arg1,String arg2,String arg3,String arg4,String arg5,String arg6,int arg7,int arg8)
	{
    	id = arg0;
    	url =arg1 ;
    	card =arg2;
    	to  =arg3;
    	from =arg4;
    	name =arg5;
    	icon =arg6;     
    	position=arg7;
    	sort =arg8;
	}

}
