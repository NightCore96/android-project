package com.android.sip.rtlab.teasurebowl;


import java.util.HashMap;
import java.util.Hashtable;
import android.graphics.Bitmap;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
 
//QR CODE �ѽXlib

public final class Zxingcoder {
	
	 public static  String decode(Bitmap bMap)
    {
		String contents = null;
		int[] intArray = new int[bMap.getWidth()*bMap.getHeight()]; 
		//copy pixel data from the Bitmap into the 'intArray' array  
		 
		bMap.getPixels(intArray, 0, bMap.getWidth(), 0, 0, bMap.getWidth(), bMap.getHeight());
		
		LuminanceSource source = new RGBLuminanceSource(bMap.getWidth(), bMap.getHeight(), intArray);
		BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
		// use this otherwise ChecksumException
		Reader reader = new MultiFormatReader();
		try
		{
			Hashtable<DecodeHintType, Object> decodeHints = new Hashtable<DecodeHintType, Object>();
			decodeHints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);
			decodeHints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
			Result result = reader.decode(bitmap, decodeHints);
		//    Result result = reader.decode(bitmap);
		    contents = result.getText();
		}
		catch (NotFoundException e)
		{
		    e.printStackTrace();
		}
		catch (ChecksumException e)
		{
		    e.printStackTrace();
		}
		catch (FormatException e)
		{
		    e.printStackTrace();
		}
		return contents;
    }
	
	  public static Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int desiredWidth, int desiredHeight) throws WriterException
	    {
		if (contents.length() == 0) return null;
		final int WHITE = 0xFFFFFFFF;
		final int BLACK = 0xFF000000;
		HashMap<EncodeHintType,Object> hints = null;
		String encoding = null;
		for (int i = 0; i < contents.length(); i++)
		{
		    if (contents.charAt(i) > 0xFF)
		    {
			encoding = "UTF-8";
			break;
		    }
		}
		if (encoding != null)
		{
		    hints = new HashMap<EncodeHintType, Object>();
		    hints.put(EncodeHintType.CHARACTER_SET, encoding);
		}
		MultiFormatWriter writer = new MultiFormatWriter();
		BitMatrix result = writer.encode(contents, format, desiredWidth, desiredHeight, hints);
		int width = result.getWidth();
		int height = result.getHeight();
		int[] pixels = new int[width * height];
		for (int y = 0; y < height; y++)
		{
		    int offset = y * width;
		    for (int x = 0; x < width; x++)
		    {
			pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
		    }
		}
		Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
		return bitmap;
	    }
	  
	  public static Bitmap encodeAsBitmapWithImage(String contents, BarcodeFormat format, int desiredWidth, int desiredHeight,Bitmap middimage) throws WriterException
	    {
		if (contents.length() == 0) return null;
		final int WHITE = 0xFFFFFFFF;
		final int BLACK = 0xFF000000;
		int midWidth=desiredWidth*2/14;
		int midHeight=desiredHeight*2/14;
		Bitmap image= Bitmap.createScaledBitmap(middimage, midWidth, midHeight, true);
		HashMap<EncodeHintType,Object> hints = null;
		String encoding = null;
		for (int i = 0; i < contents.length(); i++)
		{
		    if (contents.charAt(i) > 0xFF)
		    {
			encoding = "UTF-8";
			break;
		    }
		}
		if (encoding != null)
		{
		    hints = new HashMap<EncodeHintType, Object>();
		    hints.put(EncodeHintType.CHARACTER_SET, encoding);
		}
		MultiFormatWriter writer = new MultiFormatWriter();
		BitMatrix result = writer.encode(contents, format, desiredWidth, desiredHeight, hints);
		int width = result.getWidth();
		int height = result.getHeight();
		int[] pixels = new int[width * height];
		for (int y = 0; y < height; y++)
		{
		    int offset = y * width;
		    for (int x = 0; x < width; x++)
		    {
			pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
		    }
		}

		
		Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);

		return 	CommonUtilities.combineBitmap(bitmap, CommonUtilities.addFrame(image));
	    }
}
