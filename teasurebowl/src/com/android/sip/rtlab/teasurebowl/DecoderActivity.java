package com.android.sip.rtlab.teasurebowl;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.google.zxing.common.StringUtils;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


//解碼qr code image的Activity 


@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint("NewApi")
public class DecoderActivity extends Activity
{
	
	private int icon_num ;
	private int icon_allnum ;
	private TbowlDB dbHelper;//通訊錄
	
	 private Bitmap  qrcodebitmap =null;  
	 
	 private static String URL_CODE="";
	 
	 private AlertDialog inputDialog;
	 
	 private	SharedPreferences settings;
	 
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.decode_layout);
	        
	        
	      //通訊錄DB
			dbHelper = new TbowlDB(this);
			
			
			//讀取設定檔部份 
	  		settings = getSharedPreferences("shortcut_setting", 0);
			icon_num = settings.getInt("icon_num",-2);//紀錄總有多少ICON在主頁面
			icon_allnum = settings.getInt("icon_allnum",-2);//紀錄總有多少ICON
			if(icon_num==-2)
			{
				icon_num = -1;
				settings.edit()
				.putInt("icon_num", icon_num)
				.commit();
			}
			if(icon_allnum==-2)
			{
				icon_allnum = -1;
				settings.edit()
				.putInt("icon_allnum", icon_allnum)
				.commit();
			}
			Intent intent = getIntent();
		    String action = intent.getAction();
		    String type = intent.getType();
		    
		    if (Intent.ACTION_SEND.equals(action) && type != null)
		    {
		    	if (type.startsWith("image/"))
		    	{
		    	      Log.e("ACTION_SEND+++++", "ACTION_SEND+++++");
		            handleSendImage(intent); // Handle single image being sent
		            
		    	}
		    } 
		    else if(Intent.ACTION_VIEW.equals(action) && type != null)
		    {
		        // Handle other intents, such as being started from the home screen
		        Log.e("ACTION_VIEW+++++","ACTION_VIEW+++++");
		    	 handleViewImages(intent);
		    }
			
		
	        
	        
	 }

	 
	 //送圖進來的動作是ACTION_VIEW 的時候
	 
	@SuppressLint("NewApi")
	private void handleViewImages(Intent intent) {
			// TODO Auto-generated method stub
			Uri imageUrl;
			Intent imageintent=intent;  
			imageUrl=imageintent.getData(); 
			String path =imageUrl.getEncodedPath();
			qrcodebitmap= null;
	        qrcodebitmap=  BitmapFactory.decodeFile(path);
	    //    qrcodebitmap= "http://rtlabdemotest?"+ qrcodebitmap
	        
	        if(qrcodebitmap!=null)
	        {
	      //  	 ImageView qr_image= (ImageView)findViewById(R.id.qrcodeimage);
	 	  //      qr_image.setImageBitmap(qrcodebitmap);
	 	        
	 	      String qrString = Zxingcoder.decode(qrcodebitmap);
	 	     Log.e("qrString~~~~~~~~~~~",qrString);
	 	     qrString= URL_CODE+ qrString;
	 	     
	 	      if(qrString!=null)
	 	      {
	 	//    	  TextView qr_text= (TextView)findViewById(R.id.encodetext);
	 	    			Uri myURI = Uri.parse(qrString); 
	 	    			String[] AfterSplit = qrString.split(",");
	 	    			
	 	    			
						//取得URL中的Query String參數
				  		String tValue = CommonUtilities.CELLWEB_URL;
				  		final String gValue= myURI.getQueryParameter("giftcard");
				  		
				  		String cValue = AfterSplit[0];
				  		String fValue =  AfterSplit[1];
				  		String toValue =  AfterSplit[2];
				  		String nValue =  AfterSplit[3];
				  		String iValue =  CommonUtilities.IMAGEWEB_URL+AfterSplit[4];
				  		
				 // 	 Toast.makeText(DecoderActivity.this, "取得URL中的Query String參數", Toast.LENGTH_LONG).show();
		 			  	if(cValue!=null&&cValue!=null&&cValue!=null&&cValue!=null&&cValue!=null&&gValue==null)
				  		{
			 			  	   Log.e("cValue+++++", String.valueOf(cValue));
			 			  	Log.e("toValue+++++", String.valueOf(toValue));
			 				Log.e("iValue+++++", String.valueOf(iValue));
			 			  	 Cursor cursor= dbHelper.getIcon(cValue, toValue);
			 			  //  Log.e("rows_num", String.valueOf(cursor.getCount()));
			 			  	int rows_num = cursor.getCount();
			 			   Log.e("rows_num+++++", String.valueOf(rows_num));
			 			  	cursor.close();
			 			  	if(rows_num != 0)
			 			  	{
			 			  		Toast.makeText(DecoderActivity.this, "重複的Icon", Toast.LENGTH_LONG).show();
		 			        	  Intent intent1 = new Intent();
		 			        	  intent1.setClass(DecoderActivity.this,MainActivity.class);
		 			        	  startActivity(intent1); 
		 			        	 DecoderActivity.this.finish(); 
			 			  	}
			 			  	else
			 			  	{
			 			  		icon_num++;
			 			  		settings.edit()
			 					.putInt("icon_num", icon_num)
			 					.commit();
			 			  		icon_allnum++;
			 			  		settings.edit()
			 					.putInt("icon_allnum", icon_allnum)
			 					.commit();
			 			  		long dbid =dbHelper.createIcon(icon_allnum, tValue, cValue, toValue, fValue, nValue, iValue,-1,icon_num);
			 			  		Icondata i =new Icondata(icon_allnum,tValue,cValue,toValue,fValue,nValue,iValue,-1,icon_num);
			 		//	  	 Toast.makeText(DecoderActivity.this, "123", Toast.LENGTH_LONG).show();			 			  		
			 			  	//	textView1.setText(tValue);
			 			  		new AsyncTask<Icondata, Void, Bitmap>() 
			 			        {
			 			          @Override
			 			          protected Bitmap doInBackground(Icondata... params) 
			 			          {
			 			        	 Icondata t = params[0];
			 			            return CommonUtilities.setupShortcut(t.url,t.name,t.icon,t.card,t.to,t.from,DecoderActivity.this) ;
			 			          }
			 			          @Override
			 			          protected void onPostExecute(Bitmap result) 
			 			          {
			 			        	  Intent intent = new Intent();
			 			        	  intent.setClass(DecoderActivity.this,MainActivity.class);
			 			        	  startActivity(intent); 
			 			        	 DecoderActivity.this.finish(); 
			 			          }       
			 			        }.execute(i);
			 			  	}
		 			  }
			 		else if(gValue!=null)
			 		{
	 			  		new AsyncTask<String, Void, CardIfonData>() 
	 			        {
	 			          @Override
	 			          protected CardIfonData doInBackground(String... params) 
	 			          {
	 			        	 //CardIfonData cid; 
	 			        	  	CardIfonData cid=WebService.getCardcurrentPoint(params[0]);
								
								return cid;
	 			          }
	 			          @Override
	 			          protected void onPostExecute(CardIfonData result) 
	 			          {
	 			        	  
	 			        	  if(result.currentPoint==-1)//沒錢或是卡號有問題
			              		{
			            			Toast.makeText(DecoderActivity.this, "卡號："+gValue+"請在檢查一遍"+result.status, Toast.LENGTH_LONG).show();
			              		}
			            		else
			            		{
			            			dbHelper.createCard(gValue);
			            			Toast.makeText(DecoderActivity.this, "已將卡號："+gValue+"存為你的庫存金", Toast.LENGTH_LONG).show();
			            		}
	 			        	  Intent intent = new Intent();
	 			        	  intent.setClass(DecoderActivity.this,MainActivity.class);
	 			        	  startActivity(intent); 
	 			        	 DecoderActivity.this.finish(); 
	 			          }       
	 			        }.execute(gValue);
			 		}
			  		else
			  		{
			  			Toast.makeText(DecoderActivity.this, "解碼錯誤，請在確認一遍!", Toast.LENGTH_LONG).show();
						  Intent i = new Intent();
			        	  i.setClass(DecoderActivity.this,MainActivity.class);
			        	  startActivity(i ); 
			        	 DecoderActivity.this.finish(); 
			  		}
	 	       }
	 	      else
	 	      {
	 	    	 Toast.makeText(DecoderActivity.this, "QRCode解碼錯誤，請在確認一遍", Toast.LENGTH_LONG).show();
				  Intent i = new Intent();
	        	  i.setClass(DecoderActivity.this,MainActivity.class);
	        	  startActivity(i ); 
	        	 DecoderActivity.this.finish(); 
	 	      }
	        }
		}


	
	 //送圖進來的動作是ACTION_SEND 的時候
	private void handleSendImage(Intent intent) {
		// TODO Auto-generated method stub
		Uri imageUrl;
		
		imageUrl=(Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
		 Log.e("getScheme+++++", imageUrl.getScheme());
		 Log.e("getHost+++++", imageUrl.getHost());
		 String getScheme= imageUrl.getScheme();
		 String getHost= imageUrl.getHost();
		 qrcodebitmap= null;
		if(getScheme.startsWith("content")&&getHost.startsWith("gmail-ls"))
		{
			Log.e("GAMAIL", String.valueOf(imageUrl));
			// Intent theIntent = getIntent();
		  try 
		    {
			  InputStream attachment = getContentResolver().openInputStream(imageUrl);
				  if (attachment == null)
			            Log.e("onCreate", "cannot access mail attachment");
			        else
			        {
			            FileOutputStream tmp = new FileOutputStream(Environment.getExternalStorageDirectory().toString()+"/teasurebowl/attachment.png");
			            byte []buffer = new byte[1024];
			            while (attachment.read(buffer) > 0)
			                tmp.write(buffer);
	
			            tmp.close();
			            attachment.close();
			            Log.e("onCreate", Environment.getExternalStorageDirectory().toString()+"/teasurebowl/attachment.png");
			            qrcodebitmap=  CommonUtilities.convertToBitmap(Environment.getExternalStorageDirectory().toString()+"/teasurebowl/attachment.png",750,750);
			        }
		    } 
		    catch (FileNotFoundException e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		    } catch (IOException e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		    }
		  
		}
		else
		{
			if(imageUrl != null )
			{
				String path =imageUrl.getEncodedPath();
				Log.e("path+++++", String.valueOf(imageUrl));
		        qrcodebitmap= BitmapFactory.decodeFile(path);
		       
			}
			else
			{
				Toast.makeText(DecoderActivity.this, "錯誤，請在確認一遍!!", Toast.LENGTH_LONG).show();
	  			
	 			 Intent i = new Intent();
	 			 i.setClass(DecoderActivity.this,MainActivity.class);
	 			 startActivity(i ); 
	 			 DecoderActivity.this.finish(); 
			}
		}

		
        
        
        if(qrcodebitmap!=null)
        {
      //  	 ImageView qr_image= (ImageView)findViewById(R.id.qrcodeimage);
 	  //      qr_image.setImageBitmap(qrcodebitmap);
 	        
 	      String qrString = Zxingcoder.decode(qrcodebitmap);
 	     Log.e("qrString~@@@@@@",qrString);
 	     qrString= URL_CODE+ qrString;
 	     
 	      if(qrString!=null)
 	      {
 	//    	  TextView qr_text= (TextView)findViewById(R.id.encodetext);
 	    		Uri myURI = Uri.parse(qrString); 
 	    		 Log.e("myURI+++++", String.valueOf(myURI));
 	//    	 qr_text.setText(myURI.toString());
 	    		String[] AfterSplit = qrString.split(",",-1);
 	    		

					//取得URL中的Query String參數
			  		String tValue = CommonUtilities.CELLWEB_URL;
			  		final String gValue= myURI.getQueryParameter("giftcard");
			  		
			  		String cValue = AfterSplit[0];
			  		String fValue =  AfterSplit[1];
			  		String toValue =  AfterSplit[2];
			  		String nValue =  AfterSplit[3];
			  		String iValue =  CommonUtilities.IMAGEWEB_URL+AfterSplit[4];
			  		
			 // 	 Toast.makeText(DecoderActivity.this, "取得URL中的Query String參數", Toast.LENGTH_LONG).show();
	 			  	if(cValue!=null&&cValue!=null&&cValue!=null&&cValue!=null&&cValue!=null&&gValue==null)
			  		{
			 			  	   Log.e("cValue+++++", String.valueOf(cValue));
			 			   	Log.e("toValue+++++", String.valueOf(toValue));
			 			  	 Cursor cursor= dbHelper.getIcon(cValue, toValue);
			 			///	  Log.e("rows_num", String.valueOf(cursor.getCount()));
			 			  	 
			 			  	int rows_num = cursor.getCount();
			 			   Log.e("rows_num+++++", String.valueOf(rows_num));
			 			  	cursor.close();
			 			  	if(rows_num != 0)
			 			  	{
			 			  		Toast.makeText(DecoderActivity.this, "重複的Icon", Toast.LENGTH_LONG).show();
			 			  	  Intent intent1 = new Intent();
	 			        	  intent1.setClass(DecoderActivity.this,MainActivity.class);
	 			        	  startActivity(intent1); 
	 			        	 DecoderActivity.this.finish(); 
			 			  	}
			 			  	else
			 			  	{
			 			  		icon_num++;
			 			  		settings.edit()
			 					.putInt("icon_num", icon_num)
			 					.commit();
			 			  		icon_allnum++;
			 			  		settings.edit()
			 					.putInt("icon_allnum", icon_allnum)
			 					.commit();
			 			  		long dbid =dbHelper.createIcon(icon_allnum, tValue, cValue, toValue, fValue, nValue, iValue,-1,icon_num);
			 			  		Icondata i =new Icondata(icon_allnum,tValue,cValue,toValue,fValue,nValue,iValue,-1,icon_num);
			 			  		new AsyncTask<Icondata, Void, Bitmap>() 
			 			        {
			 			          @Override
			 			          protected Bitmap doInBackground(Icondata... params) 
			 			          {
			 			        	 Icondata t = params[0];
			 			            return CommonUtilities.setupShortcut(t.url,t.name,t.icon,t.card,t.to,t.from,DecoderActivity.this) ;
			 			          }
			 			          @Override
			 			          protected void onPostExecute(Bitmap result) 
			 			          {
			 			        	  Intent intent = new Intent();
			 			        	  intent.setClass(DecoderActivity.this,MainActivity.class);
			 			        	  startActivity(intent); 
			 			        	 DecoderActivity.this.finish(); 
			 			          }       
			 			        }.execute(i);
			 			  	}
				  		}
				 		else if(gValue!=null)
				 		{
				 			new AsyncTask<String, Void, CardIfonData>() 
		 			        {
		 			          @Override
		 			          protected CardIfonData doInBackground(String... params) 
		 			          {
		 			        	 //CardIfonData cid; 
		 			        	  	CardIfonData cid=WebService.getCardcurrentPoint(params[0]);
									
									return cid;
		 			          }
		 			          @Override
		 			          protected void onPostExecute(CardIfonData result) 
		 			          {
		 			        	   
		 			        	  if(result.currentPoint==-1)//沒錢或是卡號有問題
				              		{
				            			Toast.makeText(DecoderActivity.this, "卡號："+gValue+"請在檢查一遍"+result.status, Toast.LENGTH_LONG).show();
				              		}
				            		else
				            		{
				            			dbHelper.createCard(gValue);
				            			Toast.makeText(DecoderActivity.this, "已將卡號："+gValue+"存為你的庫存金", Toast.LENGTH_LONG).show();
				            		}
		 			        	  Intent intent = new Intent();
		 			        	  intent.setClass(DecoderActivity.this,MainActivity.class);
		 			        	  startActivity(intent); 
		 			        	 DecoderActivity.this.finish(); 
		 			          }       
		 			        }.execute(gValue);
				 		}
			  		else
			  		{
			  			Toast.makeText(DecoderActivity.this, "解碼錯誤，請在確認一遍!!", Toast.LENGTH_LONG).show();
			  			 ImageView img= (ImageView)findViewById(R.id.qrimg);
			  	    	img.setImageBitmap(qrcodebitmap);
			  		
			  		}
 	       }
 	      else
 	      {
 	    	 Toast.makeText(DecoderActivity.this, "QRCode解碼錯誤，請在確認一遍", Toast.LENGTH_LONG).show();
 	    	 ImageView img= (ImageView)findViewById(R.id.qrimg);
 	    	img.setImageBitmap(qrcodebitmap);
			//	  Intent i = new Intent();
	        //	  i.setClass(DecoderActivity.this,MainActivity.class);
	        //	  startActivity(i ); 
	        //	 DecoderActivity.this.finish(); 
 	      }
        }
	}
	
}
