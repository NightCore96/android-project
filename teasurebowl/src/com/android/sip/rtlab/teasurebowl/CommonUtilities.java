package com.android.sip.rtlab.teasurebowl;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;

//這邊是通用的變數或是函式

public final class CommonUtilities {
	
	 static final String TAG = "treasurebowl";
	
	 static final int TYPE_CELLPHONE=1;
	 static final int TYPE_DELETE=2;
	 static final int TYPE_SHARE=3;
	 
	 static final float ICON_SCALE=(float) 0.4;
	 
	 static final String CELLWEB_URL="140.114.79.111"; //電話 api
	 static final String IMAGEWEB_URL="140.114.79.112:8000/media/pic/";//圖片的位置
	 static final String HELP_URL="140.114.79.108/TB_help/help.html"; //使用說明的網頁
	 static final String NEWS_URL="140.114.79.108/TB_news/news.htm";//最新消息的網頁
	 static final String HTTP="http://";
	
  	//讀取網路圖片，型態為Bitmap
	public static  Bitmap getBitmapFromURL(String imageUrl ,Context context) 
	   {
	      try 
	      {
	         URL url = new URL(imageUrl);
	        
	         HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	         connection.setDoInput(true);
	         connection.setConnectTimeout(3000); //set timeout to 1 seconds
	         connection.connect();
	      //   connection.setRequestMethod("HEAD");
	        
	         int status = connection.getResponseCode();
	         Bitmap bitmap=BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_launcher);
	         switch (status) {
	            case java.net.HttpURLConnection.HTTP_GATEWAY_TIMEOUT://504
	              //  System.out.println("連線網址逾時!");
	            	bitmap=BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_launcher);
	                break;
	            case java.net.HttpURLConnection.HTTP_FORBIDDEN://403
	             ///   System.out.println("連線網址禁止!");
	            	bitmap=BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_launcher);
	                break;
	            case java.net.HttpURLConnection.HTTP_INTERNAL_ERROR://500
	               // System.out.println("連線網址錯誤或不存在!");
	            	bitmap=BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_launcher);
	                break;
	            case java.net.HttpURLConnection.HTTP_NOT_FOUND://404
	               // System.out.println("連線網址不存在!");
	            	bitmap=BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_launcher);
	                break;
	            case java.net.HttpURLConnection.HTTP_OK:
	            	 InputStream input = connection.getInputStream();
	    	         bitmap = BitmapFactory.decodeStream(input);
	                break;
	            }
	         return bitmap;
	      } 
	      catch (java.net.SocketTimeoutException e) {
		    	 Bitmap bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_launcher);
		         e.printStackTrace();
		         return bmp;
	    	} 
	      catch (IOException e) 
	      {
	    	 Bitmap bmp = BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_launcher);
	         e.printStackTrace();
	         return bmp;
	      }
	   }
	
	//讀網路圖片
	@SuppressWarnings("resource")
	public static  String loadImageFromWebOperations(String url, String path) {
	    try {
	        InputStream is = (InputStream) new URL(url).getContent();

	        System.out.println(path);
	        File f = new File(path);

	        f.createNewFile();
	        FileOutputStream fos = new FileOutputStream(f);
	        try {

	            byte[] b = new byte[100];
	            int l = 0;
	            while ((l = is.read(b)) != -1)
	                fos.write(b, 0, l);

	        } catch (Exception e) {

	        }

	        return f.getAbsolutePath();
	    } catch (Exception e) {
	        System.out.println("Exc=" + e);
	        return null;

	    }
	}
	
	//製作桌面捷徑
	public static Bitmap setupShortcut(String url,String name,String icon,String card, String to ,String from,Context context) 
	{
		Intent shortcut = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
		shortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);
		Bitmap bitmap = CommonUtilities.getBitmapFromURL("http://"+icon,context);
		Bitmap scaledBitmap= Bitmap.createScaledBitmap(bitmap, 128, 128, true);
		shortcut.putExtra(Intent.EXTRA_SHORTCUT_ICON,scaledBitmap);
		shortcut.putExtra("duplicate", false);
		Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://"+url+"/call.php?ac="+card+"&from="+ from+"&to="+to));  
		System.out.println("createIcon");
		shortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, intent);
		context.sendBroadcast(shortcut);
		return bitmap;
	}

	//讀圖並設定長寬
	public  static Bitmap convertToBitmap(String path, int w, int h) 
	{
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            opts.inPreferredConfig = Bitmap.Config.ARGB_8888;
            BitmapFactory.decodeFile(path, opts);
            int width = opts.outWidth;
            int height = opts.outHeight;
            float scaleWidth = 0.f, scaleHeight = 0.f;
            if (width > w || height > h) {
                scaleWidth = ((float) width) / w;
                scaleHeight = ((float) height) / h;
            }
            opts.inJustDecodeBounds = false;
            float scale = Math.max(scaleWidth, scaleHeight);
            opts.inSampleSize = (int)scale;
            WeakReference<Bitmap> weak = new WeakReference<Bitmap>(BitmapFactory.decodeFile(path, opts));
            return Bitmap.createScaledBitmap(weak.get(), w, h, true);
        }
	
	//檢查是否有裝sd card
	public static boolean checkSDCard() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }
	

	//Bitmap to Bytes
	public static byte[] Bitmap2Bytes(Bitmap bm)
	{
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
	    return baos.toByteArray();
	}
	//drawable to Bitmap
	public static Bitmap drawable2Bitmap(Drawable drawable)
	{
	    Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(),
	            drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
	    Canvas canvas = new Canvas(bitmap);
	    drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
	    drawable.draw(canvas);
	    return bitmap;
	}
	
	//SD卡是否夠大
	public static boolean checkSDCardisFull(byte[] data) {
		String storage = Environment.getExternalStorageDirectory()
				.toString();
		StatFs fs = new StatFs(storage);
		long available = (long) (fs.getAvailableBlocks())* fs.getBlockSize();
		
		if (available < data.length) 
		{
			return true;
		}
        return false;
    }
//存檔
public static void saveBitmap(String pFolderPath, Bitmap pBitmap, String pFileName) throws IOException {
        try
        {
        	ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        	pBitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
        	
	        String _folderPath = pFolderPath;
	        File file = new File(pFolderPath);
			if(!file.exists())
			{
				file.mkdir();
			}
	       
	        File _file = new File(_folderPath +File.separator+ pFileName);
	        OutputStream _outStream  = new FileOutputStream(_file);
	
	        pBitmap.compress(Bitmap.CompressFormat.JPEG, 90, _outStream);
	     
	        _outStream.flush();
	        _outStream.close();

        }
        catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace ();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace ();
        }
    }
//自動檔名依日期
@SuppressLint("SimpleDateFormat")
public static String getTimeName() {
        Date _date = new Date(System.currentTimeMillis());
        SimpleDateFormat _sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Random _random = new Random();
        return _sdf.format(_date) + _random.nextInt(999);
    }

	//合併BITMAP
	public static Bitmap combineBitmap(Bitmap background, Bitmap foreground)
	{  
	    if (background == null) {  
	        return null;  
	    }  
	    int bgWidth = background.getWidth();  
	    int bgHeight = background.getHeight();  
	    int fgWidth = foreground.getWidth();  
	    int fgHeight = foreground.getHeight();  
	    Bitmap newmap = Bitmap  
	            .createBitmap(bgWidth, bgHeight, Config.ARGB_8888);  
	    Canvas canvas = new Canvas(newmap);  
	    canvas.drawBitmap(background, 0, 0, null);  
	    canvas.drawBitmap(foreground, (bgWidth - fgWidth) / 2,  
	            (bgHeight - fgHeight) / 2, null);  
	    canvas.save(Canvas.ALL_SAVE_FLAG);  
	    canvas.restore();  
	    return newmap;  
	}  
	
	//合併BITMAP
	public static Bitmap combineBitmap(Bitmap background, Bitmap foreground,float Fsx,float Fsy,float  Fx, float Fy)
	{  
	    if (background == null) {  
	        return null;  
	    }  
	    int bgWidth = background.getWidth();  
	    int bgHeight = background.getHeight();  
	    int fgWidth = (int) (background.getHeight()*Fsx);  
	    int fgHeight = (int) (background.getHeight()*Fsy);  
	    Bitmap temp=Bitmap.createScaledBitmap((Bitmap) foreground, fgWidth, fgHeight, true);
	    Bitmap newmap = Bitmap  
	            .createBitmap(bgWidth, bgHeight, Config.ARGB_8888);  
	    Canvas canvas = new Canvas(newmap);  
	    canvas.drawBitmap(background, 0, 0, null);  
	    canvas.drawBitmap(temp, bgWidth*Fx,  bgHeight*Fy, null);  
	    canvas.save(Canvas.ALL_SAVE_FLAG);  
	    canvas.restore();  
	    return newmap;  
	}  
	// convert InputStream to String
	public  static String getStringFromInputStream(InputStream is) {
		 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
 
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
 
		return sb.toString();
 
	}
	
	//加上邊框
	
	public static Bitmap addFrame(Bitmap bmp)  
	{  
	    int borderSize = 4;
	      
	    Bitmap bmpWithBorder = Bitmap.createBitmap(bmp.getWidth() + borderSize  
	            * 2, bmp.getHeight() + borderSize * 2, bmp.getConfig());  
	    Canvas canvas = new Canvas(bmpWithBorder);  
	    canvas.drawColor(Color.parseColor("#FFFFFF"));
	    canvas.drawBitmap(bmp, borderSize, borderSize, null);  
	    return bmpWithBorder;  
	}
	
}
