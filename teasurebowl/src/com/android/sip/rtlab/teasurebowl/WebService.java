package com.android.sip.rtlab.teasurebowl;

import java.io.IOException;
import java.net.SocketTimeoutException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

//WebService用 主要是查詢卡片餘額

public final class WebService {
	public static  final String Card_wsdk ="http://ts.kits.tw/6talkWS/services/CardServiceImpl?wsdl";
	public static final String Card_targetNamespace="http://impl.service.card.talk";
	
	private static  final String Card_APIKey = "ABE66E483DDD4CFFBF766826C842DE6446CCA2F7";
	
	private static final String Card_func_getCardInfo="getCardInfo";
	private static final String SOAP_ACTION = "http://ts.kits.tw/6talkWS/services/CardServiceImpl/";
	
	public static CardIfonData getCardcurrentPoint(String cardID)
	{
		CardIfonData cid;
		SoapObject request = new SoapObject(Card_targetNamespace, Card_func_getCardInfo);
		request.addProperty("APIKey", Card_APIKey);  
		request.addProperty("cardID", cardID);  
		 SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
	    envelope.dotNet = true;
	    envelope.implicitTypes = true;
	    envelope.setAddAdornments(false);
	    envelope.setOutputSoapObject(request);
		 HttpTransportSE ht = new HttpTransportSE(Card_wsdk,10000);
		 ht.debug=true;
		// ht.setXmlVersionTag("<!--?xml version=\"1.0\" encoding= \"UTF-8\" ?-->");
		 try 
		 {
			 ht.call(SOAP_ACTION+Card_func_getCardInfo, envelope);
			 if (envelope.getResponse() != null)  
			 {
				 SoapPrimitive resultsString = (SoapPrimitive)envelope.getResponse();

				 String status = new JSONObject(resultsString.toString()).getString("status");
			  if(status.lastIndexOf("SUCCESS")>-1)
				 {
					 new JSONObject(resultsString.toString()).getString("currentPoint");
					 int cp = new JSONObject(resultsString.toString()).getInt("currentPoint");
					 cid=new CardIfonData(status,cp);
					 return cid;					 
				 }
			  cid=new CardIfonData(status,-1);
				 return  cid;
			 }
			 cid=new CardIfonData("No Response",-1);
			 return cid;
		 }
		 catch (SocketTimeoutException t) {
	            t.printStackTrace();
	            cid=new CardIfonData(t.getMessage(),-1);
	            return cid;
	        } catch (IOException i) {
	            i.printStackTrace();
	            cid=new CardIfonData("IOException",-1);
	            return cid;
	        } catch (Exception q) {
	            q.printStackTrace();
	            cid=new CardIfonData(q.getMessage(),-1);
	            return cid;
	        }
	}
	
	
	

}
