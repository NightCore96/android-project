package com.android.sip.rtlab.teasurebowl;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
//解碼URL用

public class UrlActivity extends Activity{
	
	
	private int icon_num ;
	private int icon_allnum ;
	private TbowlDB dbHelper;//通訊錄

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.url_layout);
		
		//通訊錄DB
		dbHelper = new TbowlDB(this);
		new ProgressDialog(UrlActivity.this);
		
		
		//讀取設定檔部份 
  		SharedPreferences settings = getSharedPreferences("shortcut_setting", 0);
		icon_num = settings.getInt("icon_num",-2);//紀錄總有多少ICON
		icon_allnum = settings.getInt("icon_allnum",-2);//紀錄總有多少ICON
		if(icon_num==-2)
		{
			icon_num = -1;
			settings.edit()
			.putInt("icon_num", icon_num)
			.commit();
		}
		if(icon_allnum==-2)
		{
			icon_allnum = -1;
			settings.edit()
			.putInt("icon_allnum", icon_allnum)
			.commit();
		}
		
		//URL
		//這邊是處理url進來之後應該創立連結
		 Intent tIntent = this.getIntent();
		 
		 Uri myURI = tIntent.getData(); 
		 Log.i("myURI", myURI.toString());
		  if (myURI != null)
		  {
				//取得URL中的Query String參數
		  		String tValue = myURI.getQueryParameter("URL");
		  		String cValue = myURI.getQueryParameter("card");
		  		String toValue = myURI.getQueryParameter("to");
		  		String fValue = myURI.getQueryParameter("from");
		  		String nValue = myURI.getQueryParameter("NAME");
		  		String iValue = myURI.getQueryParameter("ICON");
		  		
		  		
			  		if(tValue!=null&&cValue!=null&&toValue!=null&&fValue!=null&&nValue!=null&&iValue!=null)
			  		{
		 			  	 Cursor cursor= dbHelper.getIcon(cValue, toValue);
		 			  	int rows_num = cursor.getCount();
		 			  	cursor.close();
		 			  	if(rows_num != 0)
		 			  	{
		 			  		Toast.makeText(UrlActivity.this, "重複的Icon", Toast.LENGTH_LONG).show();
		 			  	  Intent intent = new Intent();
 			        	  intent.setClass(UrlActivity.this,MainActivity.class);
 			        	  startActivity(intent); 
 			        	 UrlActivity.this.finish(); 
		 			  	}
		 			  	else
		 			  	{
		 			  			icon_num++;
			 			  		settings.edit()
			 					.putInt("icon_num", icon_num)
			 					.commit();
			 			  		icon_allnum++;
			 			  		settings.edit()
			 					.putInt("icon_allnum", icon_allnum)
			 					.commit();
			 			  		dbHelper.createIcon(icon_allnum, tValue, cValue, toValue, fValue, nValue, iValue,-1,icon_num);
			 			  		Icondata i =new Icondata(icon_allnum,tValue,cValue,toValue,fValue,nValue,iValue,-1,icon_num);
			
			 			  		
			 			  		
			 			  	//	textView1.setText(tValue);
			 			  		new AsyncTask<Icondata, Void, Bitmap>() 
			 			        {
			 			          @Override
			 			          protected Bitmap doInBackground(Icondata... params) 
			 			          {
			 			        	 Icondata t = params[0];
			 			            return CommonUtilities.setupShortcut(t.url,t.name,t.icon,t.card,t.to,t.from,UrlActivity.this) ;
			 			          }
			 			          @Override
			 			          protected void onPostExecute(Bitmap result) 
			 			          {
			 			        	  Intent intent = new Intent();
			 			        	  intent.setClass(UrlActivity.this,MainActivity.class);
			 			        	  startActivity(intent); 
			 			        	 UrlActivity.this.finish(); 
			 			          }       
			 			        }.execute(i);
		 			  	}
 			  }
	  		else
	  		{
	  			Toast.makeText(UrlActivity.this, "URL解碼錯誤，請在確認一遍", Toast.LENGTH_LONG).show();
	  		}
		  		
		  }
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		dbHelper.close();
		super.onDestroy();
	}
	
	

}
