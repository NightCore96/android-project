package com.android.sip.rtlab.teasurebowl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

//ICON資料庫

public class TbowlDB extends SQLiteOpenHelper{
	
	private static final String DATABASE_NAME = "teasurebowl.db";	//資料庫名稱
	private static final int DATABASE_VERSION = 1;	//資料庫版本
	
	private static final String COLUMN_ID_ICON = "_id";
	private static final String COLUMN_ICONID_ICON = "iconid"; //表示他唯一id
	private static final String COLUMN_URL_ICON = "url";
	private static final String COLUMN_CARD_ICON = "card";
	private static final String COLUMN_FROM_ICON = "phonefrom";
	private static final String COLUMN_TO_ICON = "phoneto";
	private static final String COLUMN_NAME_ICON = "name";
	private static final String COLUMN_ICONLINK_ICON = "icon";
	private static final String COLUMN_POSITION_ICON = "position"; //表示他在子頁面的位子 若是-1代表該項應在主頁面上
	private static final String COLUMN_SORT_ICON = "sort";
	
	private static final String DATABASE_TABLE_ICON = "icon_dblist";	//資料庫名稱
	
	private static final String COLUMN_ID_CARD = "_id";
	private static final String COLUMN_CARDID_CARD = "card";
	private static final String DATABASE_TABLE_CARD = "card_dblist";	//資料庫名稱
	
	

	private SQLiteDatabase db;
	
	public TbowlDB(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		db = this.getWritableDatabase();
		// TODO Auto-generated constructor stub
	}
	
	 @Override
	  public void onCreate(SQLiteDatabase db) {
	   String DATABASE_CREATE_TABLE =
	     "create table "+DATABASE_TABLE_ICON+" ("
	    		 + COLUMN_ID_ICON+ " integer primary key, "
	       		+COLUMN_ICONID_ICON+ " integer not null , "
	            +COLUMN_URL_ICON+ " text  not null, "
	            +COLUMN_CARD_ICON+ " text  not null, "
	            +COLUMN_TO_ICON+ " text  not null, "
	            +COLUMN_FROM_ICON+ " text  not null, "
	            +COLUMN_NAME_ICON+ " text  not null, "
	            +COLUMN_ICONLINK_ICON+ " text  not null,"
	            +COLUMN_POSITION_ICON + "  integer not null,"
	            +COLUMN_SORT_ICON + "  integer not null"
	         + ");";
	            db.execSQL(DATABASE_CREATE_TABLE);
	            DATABASE_CREATE_TABLE=   "create table "+DATABASE_TABLE_CARD+" ("
			    		 + COLUMN_ID_CARD+ " integer primary key, "
				            +COLUMN_CARDID_CARD+ " text  not null"
				         + ");";
				db.execSQL(DATABASE_CREATE_TABLE);
	  }

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		 if (newVersion > oldVersion) {
		 db.execSQL("DROP TABLE IF EXISTS " +DATABASE_TABLE_ICON); //刪除舊有的資料表
		 db.execSQL("DROP TABLE IF EXISTS " +DATABASE_TABLE_CARD); //刪除舊有的資料表
		 onCreate(db);
		 }
	}
	
	public Cursor getAllIcon() {
	    return db.rawQuery("SELECT * FROM "+DATABASE_TABLE_ICON, null);
	}
	public Cursor getIcon(int id,int position) throws SQLException {
		Cursor cursor = db.query(true,
				DATABASE_TABLE_ICON,				//資料表名稱
		new String[] {COLUMN_ID_ICON, COLUMN_ICONID_ICON,COLUMN_URL_ICON, COLUMN_CARD_ICON, COLUMN_TO_ICON,COLUMN_FROM_ICON, COLUMN_NAME_ICON, COLUMN_ICONLINK_ICON,COLUMN_POSITION_ICON,COLUMN_SORT_ICON},	//欄位名稱
		COLUMN_POSITION_ICON+"="+"'" + id+"'"+" and "+COLUMN_SORT_ICON+"="+"'" +position+"'" ,				//WHERE
		null, // WHERE 的參數
		null, // GROUP BY
		null, // HAVING
		null, // ORDOR BY
		null    // 限制回傳的rows數量
		);

		// 注意：不寫會出錯
		if (cursor != null) {
			cursor.moveToFirst();	//將指標移到第一筆資料
		}
		return cursor;
	}
	
	
	public Cursor getIcon(String CardId,String to) throws SQLException {
		Cursor cursor = db.query(true,
				DATABASE_TABLE_ICON,				//資料表名稱
		new String[] {COLUMN_ID_ICON, COLUMN_ICONID_ICON,COLUMN_URL_ICON, COLUMN_CARD_ICON, COLUMN_TO_ICON,COLUMN_FROM_ICON, COLUMN_NAME_ICON, COLUMN_ICONLINK_ICON,COLUMN_POSITION_ICON,COLUMN_SORT_ICON},	//欄位名稱
		COLUMN_CARD_ICON+"="+"'" + CardId +"'" +" and "+COLUMN_TO_ICON+"="+"'" +to+"'" ,				//WHERE
		null, // WHERE 的參數
		null, // GROUP BY
		null, // HAVING
		null, // ORDOR BY
		null    // 限制回傳的rows數量
		);

		// 注意：不寫會出錯
		if (cursor != null) {
			cursor.moveToFirst();	//將指標移到第一筆資料
		}
		return cursor;
	}
	
	public Cursor getIcon(int id) throws SQLException {
		Cursor cursor = db.query(true,
				DATABASE_TABLE_ICON,				//資料表名稱
		new String[] {COLUMN_ID_ICON, COLUMN_ICONID_ICON,COLUMN_URL_ICON, COLUMN_CARD_ICON, COLUMN_TO_ICON,COLUMN_FROM_ICON, COLUMN_NAME_ICON, COLUMN_ICONLINK_ICON,COLUMN_POSITION_ICON,COLUMN_SORT_ICON},	//欄位名稱
		COLUMN_ICONID_ICON+"="+"'" + id+"'" ,				//WHERE
		null, // WHERE 的參數
		null, // GROUP BY
		null, // HAVING
		null, // ORDOR BY
		null   // 限制回傳的rows數量
		);

		// 注意：不寫會出錯
		if (cursor != null) {
			cursor.moveToFirst();	//將指標移到第一筆資料
		}
		return cursor;
	}
	
	public Cursor getIconbypostion(int id) throws SQLException {
		Cursor cursor = db.query(true,
				DATABASE_TABLE_ICON,				//資料表名稱
		new String[] {COLUMN_ID_ICON, COLUMN_ICONID_ICON,COLUMN_URL_ICON, COLUMN_CARD_ICON, COLUMN_TO_ICON,COLUMN_FROM_ICON, COLUMN_NAME_ICON, COLUMN_ICONLINK_ICON,COLUMN_POSITION_ICON,COLUMN_SORT_ICON},	//欄位名稱
		COLUMN_POSITION_ICON+"="+"'" + id+"'" ,				//WHERE
		null, // WHERE 的參數
		null, // GROUP BY
		null, // HAVING
		null, // ORDOR BY
		null   // 限制回傳的rows數量
		);

		// 注意：不寫會出錯
		if (cursor != null) {
			cursor.moveToFirst();	//將指標移到第一筆資料
		}
		return cursor;
	}
	
	public long createIcon(int id,String url, String card , String phoneto, String phonefrom, String name, String icon,int position,int sort) {
		ContentValues args = new ContentValues();
		
		args.put(COLUMN_ICONID_ICON, id);
		args.put(COLUMN_URL_ICON, url);
		args.put(COLUMN_CARD_ICON, card);
		args.put(COLUMN_TO_ICON, phoneto);
		args.put(COLUMN_FROM_ICON, phonefrom);
		args.put(COLUMN_NAME_ICON, name);
		args.put(COLUMN_ICONLINK_ICON, icon);
		args.put(COLUMN_POSITION_ICON, position);
		args.put(COLUMN_SORT_ICON, sort);
		return db.insert(DATABASE_TABLE_ICON, null, args);
   }
	
	public boolean  deleteIcon(long rowId) {
	//	String id= rowId;
		return db.delete(DATABASE_TABLE_ICON,	COLUMN_ICONID_ICON+"=" + rowId,null)>0;
	}
	
	public boolean  deleteIcon(long rowId,long position) {
	//	String id= rowId;
		return db.delete(DATABASE_TABLE_ICON,	COLUMN_ICONID_ICON+"=" + "'" + rowId+"'" +" and "+COLUMN_POSITION_ICON+"="+"'" +position+"'" ,null)>0;
	}
	
	public void renewIconPostion(int oldpostion,int newpostion)
	{
		String strFilter = COLUMN_SORT_ICON+" = "+ oldpostion;
		ContentValues updata = new ContentValues();
		updata.put(COLUMN_SORT_ICON, newpostion);
		db.update(DATABASE_TABLE_ICON, updata, strFilter, null);
	}
	
	public void renewIconPostion(int oldpostion,int oldpostion1,int newpostion,int newpostion1)
	{
		String strFilter = COLUMN_POSITION_ICON+" = "+"'"  + oldpostion+"'" +" and "+COLUMN_SORT_ICON+"="+"'" +oldpostion1+"'" ;
		ContentValues updata = new ContentValues();
		updata.put(COLUMN_POSITION_ICON, newpostion);
		updata.put(COLUMN_SORT_ICON, newpostion1);
		db.update(DATABASE_TABLE_ICON, updata, strFilter, null);
	}
	
	public void renewIconName(int oldpostion,String newName)
	{
		String strFilter = COLUMN_ICONID_ICON+" = "+"'"  + oldpostion+"'" ;
		ContentValues updata = new ContentValues();
		updata.put(COLUMN_NAME_ICON, newName);
		db.update(DATABASE_TABLE_ICON, updata, strFilter, null);
	}
	
	
	public void renewIconToPhone(int oldpostion,String ToPhone)
	{
		String strFilter = COLUMN_ICONID_ICON+" = "+"'"  + oldpostion+"'" ;
		ContentValues updata = new ContentValues();
		updata.put(COLUMN_TO_ICON, ToPhone);
		db.update(DATABASE_TABLE_ICON, updata, strFilter, null);
	}
	
	
	public void renewIconFormPhone(int oldpostion,String FormPhone)
	{
		String strFilter = COLUMN_ICONID_ICON+" = "+"'"  + oldpostion+"'" ;
		ContentValues updata = new ContentValues();
		updata.put(COLUMN_FROM_ICON, FormPhone);
		db.update(DATABASE_TABLE_ICON, updata, strFilter, null);
	}
	
	
	public Cursor getAllCard() {
	    return db.rawQuery("SELECT * FROM "+DATABASE_TABLE_CARD, null);
	}
	
	public Cursor getCard(String CardId) throws SQLException {
		Cursor cursor = db.query(true,
				DATABASE_TABLE_CARD,				//資料表名稱
		new String[] {COLUMN_ID_CARD, COLUMN_CARDID_CARD},	//欄位名稱
		COLUMN_CARDID_CARD+"=" + CardId,				//WHERE
		null, // WHERE 的參數
		null, // GROUP BY
		null, // HAVING
		null, // ORDOR BY
		"1"  // 限制回傳的rows數量
		);

		// 注意：不寫會出錯
		if (cursor != null) {
			cursor.moveToFirst();	//將指標移到第一筆資料
		}
		return cursor;
	}

	public long createCard(String card ) {
		ContentValues args = new ContentValues();

		args.put(COLUMN_CARDID_CARD, card);


		return db.insert(DATABASE_TABLE_CARD, null, args);
   }
	
	public boolean  deleteCard(String CardId) {
		//	String id= rowId;
			return db.delete(DATABASE_TABLE_CARD,	COLUMN_CARDID_CARD+"=" + CardId,null)>0;
		}
	
	
 @Override   
   public void onOpen(SQLiteDatabase db) {     
           super.onOpen(db);       
           // TODO 每次成功打開數據庫後首先被執行     
       } 
	 
	   @Override
	        public synchronized void close() {
		   		db.close();
		   		super.close();
	        }
}
