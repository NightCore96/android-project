package com.RTLab.thor;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends Activity implements SurfaceHolder.Callback {

	private int cernet_menu_id=-1;//當下menu

	//螢幕長寬
	private int Width;
	private int Height;
	
	//影片部分
	private MediaPlayer mediaPlayer;
	private SurfaceView surfaceView;
	private SurfaceHolder surfaceHolder;
	private boolean isPaused = true;  // is it Paused?
	
	//menu type
	private static final int INDEX_TYPE = 0;//開始畫面
	private static final int MAIN_MEUN_TYPE = 1;//主選單
	private static final int FISSION_MEUN_TYPE = 2;//
	private static final int NEUTRON_MEUN_TYPE = 3;//
	private static final int THOR_APPLI_MEUN_TYPE = 4;//
	private static final int THOR_INFO_MEUN_TYPE = 5;//THOR資訊
	private static final int HELP_TYPE = 6;//THOR資訊
	private static final int NULL_TYPE = -1;//OTHER
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//FULLSCREEN
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//
		
		//鎖住螢幕方向
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		//
		
		
		
		setContentView(R.layout.main);
		
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		Width = dm.widthPixels;
		Height = dm.heightPixels;
		
		
		ImageView playView=(ImageView)findViewById(R.id.play_View);
		RelativeLayout.LayoutParams paramsplayView;
		paramsplayView = new RelativeLayout.LayoutParams(400*Width/1600, 400*Height/2560);
		paramsplayView.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
		paramsplayView.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE);
		paramsplayView.bottomMargin =1000*Height/2560 ;
		playView.setLayoutParams(paramsplayView);
		
		surfaceView = (SurfaceView) findViewById(R.id.mediaplay_view);
		RelativeLayout.LayoutParams paramssurfaceView;
		paramssurfaceView = new RelativeLayout.LayoutParams(1300*Width/1600, 1300*Height/2560);
		paramssurfaceView.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
		paramssurfaceView.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE);
		paramssurfaceView.bottomMargin =520*Height/2560 ;
		surfaceView.setLayoutParams(paramssurfaceView);
		surfaceHolder = surfaceView.getHolder();
		surfaceHolder.addCallback(this);
		 
		surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		mediaPlayer = null; //initial  
				
		surfaceView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ImageView playView=(ImageView)findViewById(R.id.play_View);
				if(isPaused==false)  
			     {
					mediaPlayer.pause();
					isPaused=true;
					Toast.makeText(MainActivity.this,"播放暫停", Toast.LENGTH_SHORT).show();	
					playView.setVisibility(ImageView.VISIBLE);
			     }
				else
				{
					mediaPlayer.start();
					isPaused=false;
					Toast.makeText(MainActivity.this,"播放開始", Toast.LENGTH_SHORT).show();
					playView.setVisibility(ImageView.INVISIBLE);
				}
			}
			});
		

		
	
		//startbutton
		ImageButton startbutton=(ImageButton)findViewById(R.id.start_button);
		startbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				defineMeunType(MAIN_MEUN_TYPE);
			}
		});
		RelativeLayout.LayoutParams paramsstartbutton;
		paramsstartbutton = new RelativeLayout.LayoutParams(600*Width/1600, 600*Height/2560);
		paramsstartbutton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
		paramsstartbutton.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE);
		paramsstartbutton.bottomMargin =480*Height/2560 ;
		startbutton.setLayoutParams(paramsstartbutton);
		
		ImageButton helpbutton=(ImageButton)findViewById(R.id.help_button);
		helpbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				helpMesg();
			}
		});
		RelativeLayout.LayoutParams paramshelpbutton;
		paramshelpbutton = new RelativeLayout.LayoutParams(600*Width/1600, 300*Height/2560);
		paramshelpbutton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
		paramshelpbutton.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);
		paramshelpbutton.rightMargin =100*Width/1600;
		paramshelpbutton.bottomMargin =180*Height/2560 ;
		helpbutton.setLayoutParams(paramshelpbutton);
		
		//AR 部分
		ImageButton  arbutton=(ImageButton)findViewById(R.id.ar_button);
		arbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ARfunction();
			}
		});
		RelativeLayout.LayoutParams paramsarbutton;
		paramsarbutton = new RelativeLayout.LayoutParams(600*Width/1600, 300*Height/2560);
		paramsarbutton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
		paramsarbutton.addRule(RelativeLayout.ALIGN_PARENT_LEFT,RelativeLayout.TRUE);
		paramsarbutton.leftMargin =100*Width/1600;
		paramsarbutton.bottomMargin =180*Height/2560 ;
		arbutton.setLayoutParams(paramsarbutton);
		
		//fissionbutton
		ImageButton  fissionbutton=(ImageButton)findViewById(R.id.fission_button);
		fissionbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				defineMeunType(FISSION_MEUN_TYPE);
			}
		});
		RelativeLayout.LayoutParams paramsfissionbutton;
		paramsfissionbutton = new RelativeLayout.LayoutParams(800*Width/1600, 390*Height/2560);
		paramsfissionbutton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
		paramsfissionbutton.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE);
		paramsfissionbutton.bottomMargin =640*Height/2560 ;
		fissionbutton.setLayoutParams(paramsfissionbutton);
		
		
		
		//thorapplibutton
		ImageButton  thorapplibutton=(ImageButton)findViewById(R.id.thor_appli_button);
		thorapplibutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				defineMeunType(THOR_APPLI_MEUN_TYPE);
			}
		});
		RelativeLayout.LayoutParams paramsthorapplibutton;
		paramsthorapplibutton = new RelativeLayout.LayoutParams(800*Width/1600, 390*Height/2560);
		paramsthorapplibutton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
		paramsthorapplibutton.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE);
		paramsthorapplibutton.bottomMargin =1140*Height/2560 ;
		thorapplibutton.setLayoutParams(paramsthorapplibutton);
		
		//thorinfobutton
		ImageButton  thorinfobutton=(ImageButton)findViewById(R.id.thor_info_button);
		thorinfobutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				defineMeunType(THOR_INFO_MEUN_TYPE);
			}
		});
		RelativeLayout.LayoutParams paramsthorinfobutton;
		paramsthorinfobutton = new RelativeLayout.LayoutParams(800*Width/1600, 390*Height/2560);
		paramsthorinfobutton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
		paramsthorinfobutton.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE);
		paramsthorinfobutton.bottomMargin =1640*Height/2560 ;
		 thorinfobutton.setLayoutParams(paramsthorinfobutton);
		
				
		//neutronbutton
		ImageButton  neutronbutton=(ImageButton)findViewById(R.id.neutron_button);
		neutronbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				defineMeunType(NEUTRON_MEUN_TYPE);
			}
		});
		RelativeLayout.LayoutParams paramsneutronbutton;
		paramsneutronbutton = new RelativeLayout.LayoutParams(1000*Width/1600, 390*Height/2560);
		paramsneutronbutton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
		paramsneutronbutton.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE);
		paramsneutronbutton.bottomMargin =140*Height/2560 ;
		neutronbutton.setLayoutParams(paramsneutronbutton);
		
		ImageButton  backindexbutton1=(ImageButton)findViewById(R.id.back_index_1_button);
		backindexbutton1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				defineMeunType(INDEX_TYPE);
			}
		});
		RelativeLayout.LayoutParams paramsbackindexbutton1;
		paramsbackindexbutton1 = new RelativeLayout.LayoutParams(500*Width/1600, 300*Height/2560);
		paramsbackindexbutton1.addRule(RelativeLayout.ALIGN_PARENT_TOP,RelativeLayout.TRUE);
		paramsbackindexbutton1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);
		paramsbackindexbutton1.topMargin =300*Height/2560 ;
		paramsbackindexbutton1.rightMargin =80*Width/1600;
		backindexbutton1.setLayoutParams(paramsbackindexbutton1);
		
		ImageButton  backmainmeunbutton=(ImageButton)findViewById(R.id.back_mainmeun_button);
		backmainmeunbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				defineMeunType(MAIN_MEUN_TYPE);
			}
		});
		RelativeLayout.LayoutParams paramsbackmainmeunbutton;
		paramsbackmainmeunbutton = new RelativeLayout.LayoutParams(500*Width/1600, 300*Height/2560);
		paramsbackmainmeunbutton.addRule(RelativeLayout.ALIGN_PARENT_TOP,RelativeLayout.TRUE);
		paramsbackmainmeunbutton.addRule(RelativeLayout.ALIGN_PARENT_LEFT,RelativeLayout.TRUE);
		paramsbackmainmeunbutton.topMargin =300*Height/2560 ;
		paramsbackmainmeunbutton.leftMargin =80*Width/1600;
		backmainmeunbutton.setLayoutParams(paramsbackmainmeunbutton);
		
		ImageButton previousbutton=(ImageButton)findViewById(R.id.previous_button);
		previousbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				previousMeun();
			}
		});
		RelativeLayout.LayoutParams paramspreviousbutton;
		paramspreviousbutton = new RelativeLayout.LayoutParams(650*Width/1600, 360*Height/2560);
		paramspreviousbutton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
		paramspreviousbutton.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);
		paramspreviousbutton.bottomMargin =50*Height/2560 ;
		paramspreviousbutton.rightMargin =80*Width/1600;
		previousbutton.setLayoutParams(paramspreviousbutton);
		
		ImageButton nextbutton=(ImageButton)findViewById(R.id.next_button);
		nextbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				nextMeun();
			}
		});
		RelativeLayout.LayoutParams paramsnextbutton;
		paramsnextbutton = new RelativeLayout.LayoutParams(650*Width/1600, 360*Height/2560);
		paramsnextbutton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE);
		paramsnextbutton.addRule(RelativeLayout.ALIGN_PARENT_LEFT,RelativeLayout.TRUE);
		paramsnextbutton.bottomMargin =50*Height/2560 ;
		paramsnextbutton.leftMargin =80*Width/1600;
		nextbutton.setLayoutParams(paramsnextbutton);
		
		

		
		

		
		
		
		//設定當前模式	
		defineMeunType(INDEX_TYPE);
		
	}

	
	///////////////////////////////////////////////
	//這邊讓你寫連到AR的地方
	
	protected void ARfunction() {
		// TODO Auto-generated method stub
		//這邊讓你寫連到AR的地方
	}

	/////////////////////////////////////////////



	protected void helpMesg() {
		// TODO Auto-generated method stub
		defineMeunType(HELP_TYPE);
	}



	protected void nextMeun() {
		// TODO Auto-generated method stub
		
		isPaused = true; 
		switch(cernet_menu_id)
		{
		case INDEX_TYPE:
			defineMeunType(MAIN_MEUN_TYPE);
			defineMeunType(INDEX_TYPE);
		break;
		case MAIN_MEUN_TYPE:
			defineMeunType(MAIN_MEUN_TYPE);
			defineMeunType(MAIN_MEUN_TYPE);
		break;
		case FISSION_MEUN_TYPE:
			defineMeunType(MAIN_MEUN_TYPE);
			defineMeunType(NEUTRON_MEUN_TYPE);
	    break;
		case NEUTRON_MEUN_TYPE :
			defineMeunType(MAIN_MEUN_TYPE);
			defineMeunType(THOR_INFO_MEUN_TYPE);
	    break;
		case THOR_INFO_MEUN_TYPE:
			defineMeunType(MAIN_MEUN_TYPE);
			defineMeunType(THOR_APPLI_MEUN_TYPE);
		   break;
		case THOR_APPLI_MEUN_TYPE:
			defineMeunType(MAIN_MEUN_TYPE);
			defineMeunType(FISSION_MEUN_TYPE);
		break;
		}
		
	}



	protected void previousMeun() {
		// TODO Auto-generated method stub
		isPaused = true; 
		switch(cernet_menu_id)
		{
		case INDEX_TYPE:
			defineMeunType(MAIN_MEUN_TYPE);
			defineMeunType(INDEX_TYPE);
		break;
		case MAIN_MEUN_TYPE:
			defineMeunType(MAIN_MEUN_TYPE);
			defineMeunType(MAIN_MEUN_TYPE);
		break;
		case FISSION_MEUN_TYPE:
			defineMeunType(MAIN_MEUN_TYPE);
			defineMeunType(THOR_APPLI_MEUN_TYPE);
	    break;
		case NEUTRON_MEUN_TYPE :
			defineMeunType(MAIN_MEUN_TYPE);
			defineMeunType(FISSION_MEUN_TYPE);
	    break;
		case THOR_INFO_MEUN_TYPE:
			defineMeunType(MAIN_MEUN_TYPE);
			defineMeunType(NEUTRON_MEUN_TYPE);
		   break;
		case THOR_APPLI_MEUN_TYPE:
			defineMeunType(MAIN_MEUN_TYPE);
			defineMeunType(THOR_INFO_MEUN_TYPE);
		break;
		}
		
	}



	//定義當下畫面呈現
	protected void defineMeunType(int type)
	{
		LinearLayout  layout;
		
		//INDEX
		ImageButton startbutton;
		ImageButton arbutton;
		ImageButton helpbutton;

		
		//MAINMEUN
		ImageButton neutronbutton;
		ImageButton fissionbutton;
		ImageButton thorapplibutton;
		ImageButton thorinfobutton;
		//ImageButton backindexbutton;

		//OTHERMEUN
		ImageButton backindexbutton1;
		ImageButton backmainmeunbutton;
		ImageButton previousbutton;
		ImageButton nextbutton;
		ImageButton stopbutton;

		ImageView playView;
		
		
		layout=(LinearLayout)findViewById(R.id.MainLayout);
		
		startbutton=(ImageButton)findViewById(R.id.start_button);
		arbutton=(ImageButton)findViewById(R.id.ar_button);
		helpbutton=(ImageButton)findViewById(R.id.help_button);
		
		neutronbutton=(ImageButton)findViewById(R.id.neutron_button);
		fissionbutton=(ImageButton)findViewById(R.id.fission_button);
		thorapplibutton=(ImageButton)findViewById(R.id.thor_appli_button);
		thorinfobutton=(ImageButton)findViewById(R.id.thor_info_button);
//		backindexbutton=(ImageButton)findViewById(R.id.back_index_0_button);
		
		 backindexbutton1=(ImageButton)findViewById(R.id.back_index_1_button);
		 backmainmeunbutton=(ImageButton)findViewById(R.id.back_mainmeun_button);
		 previousbutton=(ImageButton)findViewById(R.id.previous_button);
		 nextbutton=(ImageButton)findViewById(R.id.next_button);
		 stopbutton=(ImageButton)findViewById(R.id.stop_button);
		 playView=(ImageView)findViewById(R.id.play_View);
		 
		playView.setVisibility(ImageView.INVISIBLE);
		
		switch(type)
		{
		case INDEX_TYPE:
			cernet_menu_id=INDEX_TYPE;
			
			//setBackground			
			layout.setBackgroundResource(R.drawable.thor_app_main_back);
			
			//setButtonVisibility			
			startbutton.setVisibility(ImageButton.VISIBLE);
			arbutton.setVisibility(ImageButton.VISIBLE);
			helpbutton.setVisibility(ImageButton.VISIBLE);
						
			neutronbutton.setVisibility(ImageButton.INVISIBLE);
			fissionbutton.setVisibility(ImageButton.INVISIBLE);
			thorapplibutton.setVisibility(ImageButton.INVISIBLE);
			thorinfobutton.setVisibility(ImageButton.INVISIBLE);
		//	backindexbutton.setVisibility(ImageButton.INVISIBLE);
			
			backindexbutton1.setVisibility(ImageButton.INVISIBLE);
			backmainmeunbutton.setVisibility(ImageButton.INVISIBLE);
			previousbutton.setVisibility(ImageButton.INVISIBLE);
			nextbutton.setVisibility(ImageButton.INVISIBLE);
			stopbutton.setVisibility(ImageButton.INVISIBLE);
			
			surfaceView.setVisibility(SurfaceView.INVISIBLE);
			
			break;
		case MAIN_MEUN_TYPE:
			cernet_menu_id=MAIN_MEUN_TYPE;
			
			//setBackground	
	     	layout.setBackgroundResource(R.drawable.thor_app_main_meun_back);
			
			//setButtonVisibility
			startbutton.setVisibility(ImageButton.INVISIBLE);
			arbutton.setVisibility(ImageButton.INVISIBLE);
			helpbutton.setVisibility(ImageButton.INVISIBLE);
			
			neutronbutton.setVisibility(ImageButton.VISIBLE);
			fissionbutton.setVisibility(ImageButton.VISIBLE);
			thorapplibutton.setVisibility(ImageButton.VISIBLE);
			thorinfobutton.setVisibility(ImageButton.VISIBLE);
			//backindexbutton.setVisibility(ImageButton.VISIBLE);
			
			backindexbutton1.setVisibility(ImageButton.INVISIBLE);
			backmainmeunbutton.setVisibility(ImageButton.INVISIBLE);
			previousbutton.setVisibility(ImageButton.INVISIBLE);
			nextbutton.setVisibility(ImageButton.INVISIBLE);
			stopbutton.setVisibility(ImageButton.INVISIBLE);
			
			surfaceView.setVisibility(SurfaceView.INVISIBLE);
			
			break;
		case FISSION_MEUN_TYPE:
			cernet_menu_id=FISSION_MEUN_TYPE;
			//setBackground	
	     	layout.setBackgroundResource(R.drawable.thor_app_fission_meun_back);
	     	
	    	//setButtonVisibility
			startbutton.setVisibility(ImageButton.INVISIBLE);
			arbutton.setVisibility(ImageButton.INVISIBLE);
			helpbutton.setVisibility(ImageButton.INVISIBLE);
			
			neutronbutton.setVisibility(ImageButton.INVISIBLE);
			fissionbutton.setVisibility(ImageButton.INVISIBLE);
			thorapplibutton.setVisibility(ImageButton.INVISIBLE);
			thorinfobutton.setVisibility(ImageButton.INVISIBLE);
		//	backindexbutton.setVisibility(ImageButton.INVISIBLE);
			
			backindexbutton1.setVisibility(ImageButton.VISIBLE);
			backmainmeunbutton.setVisibility(ImageButton.VISIBLE);
			previousbutton.setVisibility(ImageButton.VISIBLE);
			nextbutton.setVisibility(ImageButton.VISIBLE);
			stopbutton.setVisibility(ImageButton.INVISIBLE);
			
			surfaceView.setVisibility(SurfaceView.VISIBLE);
			loadFilm(FISSION_MEUN_TYPE);
			
			break;
		case NEUTRON_MEUN_TYPE :
			cernet_menu_id=NEUTRON_MEUN_TYPE;
			//setBackground	
	     	layout.setBackgroundResource(R.drawable.thor_app_neutron_meun_back);
	     	
	    	//setButtonVisibility
			startbutton.setVisibility(ImageButton.INVISIBLE);
			arbutton.setVisibility(ImageButton.INVISIBLE);
			helpbutton.setVisibility(ImageButton.INVISIBLE);
			
			neutronbutton.setVisibility(ImageButton.INVISIBLE);
			fissionbutton.setVisibility(ImageButton.INVISIBLE);
			thorapplibutton.setVisibility(ImageButton.INVISIBLE);
			thorinfobutton.setVisibility(ImageButton.INVISIBLE);
		//	backindexbutton.setVisibility(ImageButton.INVISIBLE);
			
			backindexbutton1.setVisibility(ImageButton.VISIBLE);
			backmainmeunbutton.setVisibility(ImageButton.VISIBLE);
			previousbutton.setVisibility(ImageButton.VISIBLE);
			nextbutton.setVisibility(ImageButton.VISIBLE);
			stopbutton.setVisibility(ImageButton.INVISIBLE);
	     	
			surfaceView.setVisibility(SurfaceView.VISIBLE);
			loadFilm(NEUTRON_MEUN_TYPE);
			
			break;
		case THOR_INFO_MEUN_TYPE:
			cernet_menu_id=THOR_INFO_MEUN_TYPE;
			//setBackground	
	     	layout.setBackgroundResource(R.drawable.thor_app_thor_info_meun_back);
	     	
	    	//setButtonVisibility
			startbutton.setVisibility(ImageButton.INVISIBLE);
			arbutton.setVisibility(ImageButton.INVISIBLE);
			helpbutton.setVisibility(ImageButton.INVISIBLE);
			
			neutronbutton.setVisibility(ImageButton.INVISIBLE);
			fissionbutton.setVisibility(ImageButton.INVISIBLE);
			thorapplibutton.setVisibility(ImageButton.INVISIBLE);
			thorinfobutton.setVisibility(ImageButton.INVISIBLE);
		//	backindexbutton.setVisibility(ImageButton.INVISIBLE);
			
			backindexbutton1.setVisibility(ImageButton.VISIBLE);
			backmainmeunbutton.setVisibility(ImageButton.VISIBLE);
			previousbutton.setVisibility(ImageButton.VISIBLE);
			nextbutton.setVisibility(ImageButton.VISIBLE);
			stopbutton.setVisibility(ImageButton.INVISIBLE);
	     	
			surfaceView.setVisibility(SurfaceView.VISIBLE);
			loadFilm(THOR_INFO_MEUN_TYPE);
			
			break;
		case THOR_APPLI_MEUN_TYPE:
			cernet_menu_id=THOR_APPLI_MEUN_TYPE;
			//setBackground	
	     	layout.setBackgroundResource(R.drawable.thor_app_thor_appli_meun_back);
	     	
	    	//setButtonVisibility
			startbutton.setVisibility(ImageButton.INVISIBLE);
			arbutton.setVisibility(ImageButton.INVISIBLE);
			helpbutton.setVisibility(ImageButton.INVISIBLE);
			
			neutronbutton.setVisibility(ImageButton.INVISIBLE);
			fissionbutton.setVisibility(ImageButton.INVISIBLE);
			thorapplibutton.setVisibility(ImageButton.INVISIBLE);
			thorinfobutton.setVisibility(ImageButton.INVISIBLE);
		//	backindexbutton.setVisibility(ImageButton.INVISIBLE);
			
			backindexbutton1.setVisibility(ImageButton.VISIBLE);
			backmainmeunbutton.setVisibility(ImageButton.VISIBLE);
			previousbutton.setVisibility(ImageButton.VISIBLE);
			nextbutton.setVisibility(ImageButton.VISIBLE);
			stopbutton.setVisibility(ImageButton.INVISIBLE);
			
			surfaceView.setVisibility(SurfaceView.VISIBLE);
			loadFilm(THOR_APPLI_MEUN_TYPE);
	     	
			break;
		case HELP_TYPE :
			cernet_menu_id=HELP_TYPE;
			//setBackground	
	     	layout.setBackgroundResource(R.drawable.help_back);
	     	
	    	//setButtonVisibility
			startbutton.setVisibility(ImageButton.INVISIBLE);
			arbutton.setVisibility(ImageButton.INVISIBLE);
			helpbutton.setVisibility(ImageButton.INVISIBLE);
			
			neutronbutton.setVisibility(ImageButton.INVISIBLE);
			fissionbutton.setVisibility(ImageButton.INVISIBLE);
			thorapplibutton.setVisibility(ImageButton.INVISIBLE);
			thorinfobutton.setVisibility(ImageButton.INVISIBLE);
		//	backindexbutton.setVisibility(ImageButton.INVISIBLE);
			
			backindexbutton1.setVisibility(ImageButton.INVISIBLE);
			backmainmeunbutton.setVisibility(ImageButton.INVISIBLE);
			previousbutton.setVisibility(ImageButton.INVISIBLE);
			nextbutton.setVisibility(ImageButton.INVISIBLE);
			stopbutton.setVisibility(ImageButton.INVISIBLE);
			
			surfaceView.setVisibility(SurfaceView.VISIBLE);
			loadFilm(HELP_TYPE);
	     	
			break;
		}
		//setContentView(layout);
	}
	
	private void loadFilm(int MeunType) {
		// TODO Auto-generated method stub
		isPaused = true; 
		try{
		//		mediaPlayer.release();
				ImageView playView=(ImageView)findViewById(R.id.play_View);
				playView.setVisibility(ImageView.VISIBLE);
				switch(MeunType)
				{
				case FISSION_MEUN_TYPE:
					mediaPlayer=MediaPlayer.create(this, R.raw.fission_film);
			    break;
				case NEUTRON_MEUN_TYPE :
					mediaPlayer=MediaPlayer.create(this, R.raw.neutron_film);
			    break;
				case THOR_INFO_MEUN_TYPE:
					mediaPlayer=MediaPlayer.create(this, R.raw.thor_info_flim);
					break;
				case THOR_APPLI_MEUN_TYPE:
					mediaPlayer=MediaPlayer.create(this, R.raw.thor_history_film);
				break;
				case HELP_TYPE:
					mediaPlayer=MediaPlayer.create(this, R.raw.help_film);
				break;
				default:
					mediaPlayer=MediaPlayer.create(this, R.raw.thor_info_flim);
					break;
				}
				mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC); 
				mediaPlayer.setDisplay(surfaceHolder); 
				mediaPlayer.prepare();
				mediaPlayer.setOnCompletionListener(
						 new MediaPlayer.OnCompletionListener()   
					        {

								@Override
								public void onCompletion(MediaPlayer mp) {
									// TODO Auto-generated method stub
									try{
										mediaPlayer.release();
										isPaused = true; 
										Toast.makeText(MainActivity.this,"播放結束", Toast.LENGTH_SHORT).show();	       
									}catch (Exception e) {
										isPaused = true; 
										Toast.makeText(MainActivity.this,"播放結束", Toast.LENGTH_SHORT).show();	       
									}
								}
					        }
						);
		}catch (Exception e) {
			
			
		}
	}

	//按下BACK之後不應該跳出程式
	@Override
	public void onBackPressed() {
		isPaused = true; 
		switch(cernet_menu_id)
		{
		case INDEX_TYPE:
			super.onBackPressed();
		break;
		case MAIN_MEUN_TYPE:
			defineMeunType(INDEX_TYPE);
		break;
		case FISSION_MEUN_TYPE:
			defineMeunType(MAIN_MEUN_TYPE);
	    break;
		case NEUTRON_MEUN_TYPE :
			defineMeunType(MAIN_MEUN_TYPE);
	    break;
		case THOR_INFO_MEUN_TYPE:
			defineMeunType(MAIN_MEUN_TYPE);
		break;
		case THOR_APPLI_MEUN_TYPE:
			defineMeunType(MAIN_MEUN_TYPE);
		break;
		case HELP_TYPE:
			defineMeunType(INDEX_TYPE);
		break;
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		
	}

}
